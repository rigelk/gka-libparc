/**
 * @file parc_GroupDiffieHellman.h
 * @ingroup security
 * @brief Group Diffie Hellman primitives used in TGDH
 */
#ifndef libparc_parc_GroupDiffieHellman_h
#define libparc_parc_GroupDiffieHellman_h

#include <openssl/bn.h>
#include <parc_DiffieHellmanKeyShare.h>

struct parc_group_diffie_hellman_tree;
typedef struct parc_group_diffie_hellman_tree parcGroupDiffieHellmanTree;

typedef BIGNUM parcGroupDiffieHellmanBlindKey;

// f to generate key
// f to create bkey from key
// f to create key from a pair (k_a,bkey_b) or (bkey_a,key_b)

/**
 * Create a `parcGroupDiffieHellmanKey` instance for a virtual node.
 */
parcGroupDiffieHellmanBlindKey *parcGroupDiffieHellman_toBlindKey(PARCDiffieHellmanKeyShare *key);

/**
 * Serialize the blinded key part of a `PARCDiffieHellmanKeyShare.`
 *
 * The blinded key is saved to a `PARCBuffer` and can be used for transport if needed.
 *
 * @param [in] keyShare A `PARCDiffieHellmanKeyShare` instance.
 *
 * @return A `PARCBuffer` containing the blinded key of this key share.
 */
PARCBuffer *parcGroupDiffieHellman_SerializeBlindKey(PARCDiffieHellmanKeyShare *keyShare);

/**
 * @brief Compute the group key using the tree structure broadcast by the sponsor
 * @param tree
 */
PARCDiffieHellmanKeyShare *parcGroupDiffieHellman_ComputeGroupKeyFromTree(parcGroupDiffieHellmanTree *tree);

/**
 * @brief Create a `parcGroupDiffieHellmanKey` from a key/bkey pair
 */
PARCDiffieHellmanKeyShare *parcGroupDiffieHellman_ComputeGroupKeyFromGroup(PARCDiffieHellmanKeyShare *key,
                                                                           parcGroupDiffieHellmanBlindKey *bkey);

#endif // libparc_parc_GroupDiffieHellman_h
