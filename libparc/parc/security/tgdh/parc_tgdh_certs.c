#include <openssl/ec.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <unistd.h>

#include <parc/security/parc_Pkcs12KeyStore.h>
#include <parc/security/parc_KeyStore.h>

#include "parc_tgdh_common.h"
#include "parc_tgdh_certs.h"
#include "parc_tgdh_catch.h"

  static void _do_ssl_setup() {
    static int need_setup=1;

    if (need_setup) {
#ifdef PRINT_CERT_ERRORS
        if (cert_bio_err==NULL)
          if ((cert_bio_err=BIO_new(BIO_s_file())) != NULL)
            BIO_set_fp(cert_bio_err,stderr,BIO_NOCLOSE|BIO_FP_TEXT);
#endif
        ERR_load_crypto_strings();
        X509V3_add_standard_extensions();
        need_setup=0;
      }
  }

  /**
   * get a cert for a given member_name
   *
   * Note: This function can be replaced for one provided by the
   * program using the API. Hence, the keys can be obtained form
   * another media if necessary. The only only condition required is
   * that the function returns a pointer to a DSA structure.
   */
  int
  (gka_get) (GKA_KEY *key,
             char *member_name,
             enum GKA_KEY_TYPE type)
  {
    return _gka_get_local(key, member_name, NULL, type);
  }

  /**
   * reads a cert structure from disk depending on
   * GKA_KEY_TYPE (GKA_PARAMS, GKA_PRV, GKA_PUB)
   */
  int
  (_gka_get_local) (GKA_KEY *key,
                    char *member_name,
                    char *p12_path,
                    enum GKA_KEY_TYPE type)
  {
    char path[_POSIX_PATH_MAX];
    (p12_path == NULL) ?
        realpath( "test_dsa.p12", path ) :
        realpath( p12_path, path );

    int signal = OK;
    /*                   Name          Default message   Supertype */
    E4C_DEFINE_EXCEPTION(KeyException, "Key error.", RuntimeException);

    int pubkey_algorithm_nid;
    PARCPkcs12KeyStore *_keyStore = parcPkcs12KeyStore_Open(path, "blueberry", PARCCryptoHashType_SHA512);
    PARCKeyStore *keyStore = parcKeyStore_Create(_keyStore, PARCPkcs12KeyStoreAsKeyStore);
    PARCBuffer *_public_key = parcKeyStore_GetDEREncodedPublicKey(keyStore);
    uint8_t *b_public_key = parcByteArray_Array(parcBuffer_Array(_public_key));
    PARCBuffer *_private_key = parcKeyStore_GetDEREncodedPrivateKey(keyStore);
    uint8_t *b_private_key = parcByteArray_Array(parcBuffer_Array(_private_key));
    PARCBuffer *_cert = parcKeyStore_GetDEREncodedCertificate(keyStore);
    uint8_t *b_cert = parcByteArray_Array(parcBuffer_Array(_cert));
    X509 *cert = d2i_X509(NULL,  (const unsigned char **) &b_cert, parcBuffer_Remaining(_cert));

    TRY
    {
      _do_ssl_setup();

      if (!cert) {
        fprintf(stderr, "unable to parse certificate in memory\n");
        THROW(EXIT_FAILURE);
        }
      else
        {
          pubkey_algorithm_nid = OBJ_obj2nid(cert->cert_info->key->algor->algorithm);
          if (pubkey_algorithm_nid == NID_undef) {
              fprintf(stderr, "unable to find specified public key algorithm name.\n");
              THROW(EXIT_FAILURE);
            }
        }

      switch (type) {

        case GKA_PARAMS : {
          try{
            if (!key) { throw(KeyException, "Key is not defined"); }
            if (pubkey_algorithm_nid == NID_rsaEncryption
                || pubkey_algorithm_nid == NID_dsa)
              {
                EVP_PKEY *pkey = X509_get_pubkey(cert);
                switch(pubkey_algorithm_nid) {
                  case NID_rsaEncryption: {
                      RSA *rsa_key = pkey->pkey.rsa; // RSA struct
                      key->params.nid_params.rsa.param_rsa_n = BN_dup(rsa_key->n);
                      key->params.nid_params.rsa.param_rsa_e = BN_dup(rsa_key->e);
                      key->params.nid_params.rsa.param_rsa_d = BN_dup(rsa_key->d);
                      key->params.nid = NID_rsa;
                      RSA_free(rsa_key);
                      break;
                    }
                  case NID_dsa: {
                      DSA *dsa_key = pkey->pkey.dsa; // DSA struct
                      key->params.nid_params.dsa.param_dsa_p = BN_dup(dsa_key->p);
                      key->params.nid_params.dsa.param_dsa_g = BN_dup(dsa_key->g);
                      key->params.nid_params.dsa.param_dsa_q = BN_dup(dsa_key->q);
                      key->params.nid = NID_dsa;
                      DSA_free(dsa_key);
                      break;
                    }
                  }
              }
            }catch(BadPointerException){
              puts("param error here");
            }
          break;
        }

        case GKA_PRV : {
          key->private_key = d2i_AutoPrivateKey(NULL,
                                                (const unsigned char**) &b_private_key,
                                                parcBuffer_Remaining(_private_key));
          break;
        }

        case GKA_PUB : {
          key->public_key = (EVP_PKEY*) &b_public_key;
          break;
        }

        default:
          THROW(INVALID_CERT_TYPE);
          break;
      }
    }
    CATCH (EXIT_FAILURE) { signal=EXIT_FAILURE; }
    CATCH (INVALID_CERT_TYPE) { signal=INVALID_CERT_TYPE; }
    FINALLY
    {
      parcBuffer_Release(&_public_key);
      parcBuffer_Release(&_private_key);
      parcBuffer_Release(&_cert);
      parcPkcs12KeyStore_Release(&_keyStore);
      parcKeyStore_Release(&keyStore);
      X509_free(cert);
    }
    ETRY;

    return signal;
  }

  int
  (gka_get_cert) (X509 **cert,
                   char *member_name)
  {
    char path[_POSIX_PATH_MAX];
    (member_name == NULL) ?
      realpath( "test_dsa.p12", path ) :
      realpath( strcat(member_name,"_dsa.p12"), path );

    int signal = OK;
    PARCPkcs12KeyStore *_keyStore = parcPkcs12KeyStore_Open(path, "blueberry", PARCCryptoHashType_SHA512);
    PARCKeyStore *keyStore = parcKeyStore_Create(_keyStore, PARCPkcs12KeyStoreAsKeyStore);
    PARCBuffer *_cert = parcKeyStore_GetDEREncodedCertificate(keyStore);
    uint8_t *b_cert = parcByteArray_Array(parcBuffer_Array(_cert));
    X509 *tmp_cert = d2i_X509(NULL,  (const unsigned char **) &b_cert, parcBuffer_Remaining(_cert));

    TRY
    {
      if (!tmp_cert) {
        fprintf(stderr, "unable to parse certificate in memory\n");
        THROW(EXIT_FAILURE);
        }
      else
        {
          *cert = X509_dup(tmp_cert);
        }
    }
    CATCH (EXIT_FAILURE) { signal=EXIT_FAILURE; }
    FINALLY
    {
      parcBuffer_Release(&_cert);
      parcPkcs12KeyStore_Release(&_keyStore);
      parcKeyStore_Release(&keyStore);
      if(tmp_cert) {X509_free(tmp_cert);}
    }
    ETRY;

    return signal;
  }

  X509*
  (r_gka_get_cert) (X509 *cert,
                   char *member_name)
  {
    char path[_POSIX_PATH_MAX];
    (member_name == NULL) ?
      realpath( "test_dsa.p12", path ) :
      realpath( strcat(member_name,"_dsa.p12"), path );

    int signal = OK;
    PARCPkcs12KeyStore *_keyStore = parcPkcs12KeyStore_Open(path, "blueberry", PARCCryptoHashType_SHA512);
    PARCKeyStore *keyStore = parcKeyStore_Create(_keyStore, PARCPkcs12KeyStoreAsKeyStore);
    PARCBuffer *_cert = parcKeyStore_GetDEREncodedCertificate(keyStore);
    uint8_t *b_cert = parcByteArray_Array(parcBuffer_Array(_cert));
    X509 *tmp_cert = d2i_X509(NULL,  (const unsigned char **) &b_cert, parcBuffer_Remaining(_cert));

    TRY
    {
      if (!tmp_cert) {
        fprintf(stderr, "unable to parse certificate in memory\n");
        THROW(EXIT_FAILURE);
        }
      else
        {
          cert = X509_dup(tmp_cert);
        }
    }
    CATCH (EXIT_FAILURE) { signal=EXIT_FAILURE; }
    FINALLY
    {
      parcBuffer_Release(&_cert);
      parcPkcs12KeyStore_Release(&_keyStore);
      parcKeyStore_Release(&keyStore);
      if(tmp_cert) {X509_free(tmp_cert);}
    }
    ETRY;

    return cert;
  }
