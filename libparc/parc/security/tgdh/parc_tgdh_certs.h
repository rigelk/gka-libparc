#ifndef libparc_parc_tgdh_certs_h
#define libparc_parc_tgdh_certs_h

/** @file icnet_utils_tgdh_certs.h
 * @brief Function prototypes for retrieval of certificates used by TGDH.
 *
 * This contains the functions related to certificates and their retrieval.
 *
 * @author Pierre-Antoine Rault (rigelk)
 */

#include <openssl/x509v3.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/pem.h>

#include <parc/security/parc_Security.h>
#include <parc/security/parc_X509Certificate.h>
#include <parc/security/parc_Key.h>

#include "parc_tgdh_errors.h"
#include "parc_tgdh_common.h"
#include "parc_tgdh_certs.h"

  typedef union GKA_KEY {
    struct gka_key_params
    {
      int nid; // to know what kind of parameter we are storing
      union gka_key_nid_params {
        struct gka_key_dsa {
          BIGNUM *param_dsa_g;
          BIGNUM *param_dsa_p;
          BIGNUM *param_dsa_q; /// Order of the group (g, p, q)
        } dsa;
        struct gka_key_rsa {
          BIGNUM *param_rsa_n; /// Modulus of the public key
          BIGNUM *param_rsa_e; /// Public exponent of the public key
          BIGNUM *param_rsa_d; /// Private exponent of the public key
        } rsa;
      } nid_params;
    } params;
//    BIGNUM *param;
    EVP_PKEY *private_key;
    EVP_PKEY *public_key;
    X509 *cert;
  } GKA_KEY;



  /** @file
   * @param [out] *cert
   * @param [in]  *member_name
   * @param [in]  type
   */
  int (gka_get) (GKA_KEY *key,
                  char *member_name,
                  enum GKA_KEY_TYPE type);

  /** @file
   * @param [out] *cert
   * @param [in]  *member_name
   * @param [in]  *p12_path path to the PKCS12 key store
   * @param [in]  type
   */
  int (_gka_get_local) (GKA_KEY *key,
                        char *member_name,
                        char *p12_path,
                        enum GKA_KEY_TYPE type);

  int (gka_get_cert) (X509 **cert,
                       char *member_name); // DON'T USE, FAULTY

  X509* (r_gka_get_cert) (X509 *cert,
                       char *member_name);


#endif // libparc_parc_tgdh_certs_h
