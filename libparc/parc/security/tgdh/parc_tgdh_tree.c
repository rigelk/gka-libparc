#include "parc_tgdh.h"

#include <parc/security/parc_X509Certificate.h>
#include <parc/security/parc_CertificateFactory.h>

/* swap pointer a and b */
void gka_swap(void** a, void** b)
{
  void *tmp = *a;
  *a = *b;
  *b = tmp;
}

  /** Frees a TGDH_TREE structure */
  void
  tgdh_free_tree(KEY_TREE **tree)
  {
    if(tree == NULL) return;
    if((*tree) == NULL) return;

    if((*tree)->left != NULL)
      tgdh_free_tree(&((*tree)->left));
    if((*tree)->right != NULL)
      tgdh_free_tree(&((*tree)->right));

    tgdh_free_node(&(*tree));
  }

  /** Frees a NODE structure */
  void
  tgdh_free_node(KEY_TREE **tree)
  {

    if(tree == NULL) return;
    if((*tree) == NULL) return;

    if((*tree)->tgdh_nv != NULL){
        tgdh_free_nv(&((*tree)->tgdh_nv));
        (*tree)->tgdh_nv = NULL;
      }

    free((*tree));

    (*tree)=NULL;
  }

  /** Frees a TGDH_NV structure */
  void
  tgdh_free_nv(TGDH_NV **nv) {
    if (nv == NULL) return;
    if ((*nv) == NULL) return;
    if((*nv)->member != NULL){
        tgdh_free_gm(&((*nv)->member));
        (*nv)->member=NULL;
      }

    if ((*nv)->key != NULL){
        BN_clear_free((*nv)->key);
      }

    if ((*nv)->bkey != NULL){
        BN_clear_free((*nv)->bkey);
      }

    free((*nv));
    (*nv)=NULL;
  }

  /** Frees a TGDH_GM structure */
  void
  tgdh_free_gm(TGDH_GM **gm)
  {
    if((*gm) == NULL) return;
    if (((*gm)->member_name) != NULL) {
        free ((*gm)->member_name);
        (*gm)->member_name=NULL;
      }
    if (((*gm)->cert) != NULL) {
        X509_free((*gm)->cert);
        (*gm)->cert=NULL;
      }
    free((*gm));
    (*gm)=NULL;
  }

  /**
   * returns the pointer of the previous or the next
   * member or the first or the last member:
   *   if option is 0, this will return the pointer to the previous member
   *   if option is 1, this will return the pointer to the next member
   *     in the above two cases, tree is the starting leaf node in this
   *     searching
   *   if option is 2, this will return the pointer to the left-most
   *      leaf member
   *   if option is 3, this will return the pointer to the right-most
   *      leaf member
   *   if option is 4 and member_name is not null, this will return the
   *     pointer to the node with that name
   *   if option is 5, this will return the pointer to the root
   *   if option is 6, this will return the shallowest leaf node
   */
  KEY_TREE
  *tgdh_search_member(KEY_TREE *tree,
                      int option,
                      GKA_NAME *member_name)
  {
    KEY_TREE *tmp_tree,
             *tmp_tree1=NULL,
             *the_tree=NULL;
    int min_node=100000;

    tmp_tree = tree;

    if(member_name == NULL){
        switch (option) {
          case 0: //!< this will return the pointer to the previous member
            if(tree->tgdh_nv->member == NULL) return NULL;
            if(tree->tgdh_nv->member->member_name == NULL) return NULL;
            if(tmp_tree->parent == NULL) return NULL;
            if(tmp_tree->parent->left == NULL) return NULL;
            while(tmp_tree->parent->left == tmp_tree){
                tmp_tree = tmp_tree->parent;
                if(tmp_tree->parent == NULL) return NULL;
              }
            tmp_tree = tmp_tree->parent->left;
            while(tmp_tree->tgdh_nv->member == NULL){
                if(tmp_tree->right == NULL) return NULL;
                tmp_tree = tmp_tree->right;
              }
            if(tmp_tree->tgdh_nv->member->member_name == NULL) return NULL;
            return tmp_tree;
          case 1: //!< this will return the pointer to the next member
                  //!< in the above two cases, tree is the starting leaf node in this
                  //!< searching
            if(tree->tgdh_nv->member == NULL) return NULL;
            if(tree->tgdh_nv->member->member_name == NULL) return NULL;
            if(tmp_tree->parent == NULL) return NULL;
            if(tmp_tree->parent->right == NULL) return NULL;
            while(tmp_tree->parent->right == tmp_tree){
                tmp_tree = tmp_tree->parent;
                if(tmp_tree->parent == NULL) return NULL;
              }
            tmp_tree = tmp_tree->parent->right;
            while(tmp_tree->tgdh_nv->member == NULL){
                if(tmp_tree->left == NULL) return NULL;
                tmp_tree = tmp_tree->left;
              }
            if(tmp_tree->tgdh_nv->member->member_name == NULL) return NULL;
            return tmp_tree;
          case 2: //!< this will return the pointer to the left-most leaf member
            if(tmp_tree->left == NULL) return tmp_tree;
            while(tmp_tree->left != NULL) tmp_tree=tmp_tree->left;
            return tmp_tree;
          case 3: //!< this will return the pointer to the right-most leaf member
            if(tmp_tree->right == NULL) return tmp_tree;
            while(tmp_tree->right != NULL) tmp_tree=tmp_tree->right;
            return tmp_tree;
          case 5: //!< this will return the pointer to the root
            if(tmp_tree->parent == NULL) return tmp_tree;
            while(tmp_tree->parent != NULL) tmp_tree=tmp_tree->parent;
            return tmp_tree;
          case 6: //!< this will return the shallowest leaf node
            tmp_tree1 = tgdh_search_member(tmp_tree, 3, NULL);
            tmp_tree = tgdh_search_member(tmp_tree, 2, NULL);
            while(tmp_tree != NULL){
                if((min_node >= 0) && (tmp_tree->tgdh_nv->index < (uint)min_node)){
                    min_node = tmp_tree->tgdh_nv->index;
                    the_tree = tmp_tree;
                  }
                if(tmp_tree == tmp_tree1){
                    break;
                  }
                tmp_tree = tmp_tree->next;
              }
            return the_tree;
          default:
            return NULL;
          }
      }
    else{
        if(option==4){
            tmp_tree = tgdh_search_member(tree, 2, NULL);
            if(tmp_tree == NULL) return NULL;

            while(strcmp(tmp_tree->tgdh_nv->member->member_name,
                         member_name)!=0 ){
                if(tmp_tree->next == NULL) return NULL;
                tmp_tree = tmp_tree->next;
              }
            return tmp_tree;
          }
      }
    return NULL;
  }

  /**
   * Returns the first fit or worst fit node
   *   if option is 0, search policy is the first fit
   *   if option is 1, search policy is the best fit
   * @pre height of the joiner should be always smaller or equal to that of
   *      the joinee
   */
  KEY_TREE
  *tgdh_search_node(KEY_TREE *joiner, //!< check joiner.height <= joinee.height
                    KEY_TREE *joinee,
                    int option)
  {
    KEY_TREE *tmp_tree;

    if(joiner->tgdh_nv->height > joinee->tgdh_nv->potential) return joinee;

    tmp_tree = joinee;

    if(option==0){ //!< search policy is *first* fit, else best fit
        while(tmp_tree->tgdh_nv->joinQ == FALSE){
            if(tmp_tree->right->tgdh_nv->potential >=
               tmp_tree->left->tgdh_nv->potential){
                tmp_tree = tmp_tree->right;
              }
            else tmp_tree = tmp_tree->left;
          }
        return tmp_tree;
      }
    return NULL;
  }

  /**
   * Returns the node having the index as a child
   * @pre index should be greater than 1
   */
  KEY_TREE
  *tgdh_search_index(KEY_TREE *tree, int index)
  {
    int height=0;
    int i;
    KEY_TREE *tmp_tree;

    height = log2(index);

    tmp_tree = tree;

    if(index==1) return NULL;

    for(i=1; i<height; i++){
        if((index >> (height-i)) & 0x1){
            if(tmp_tree->right == NULL) return NULL;
            else tmp_tree = tmp_tree->right;
          }
        else{
            if(tmp_tree->left == NULL) return NULL;
            else tmp_tree = tmp_tree->left;
          }
      }

    return tmp_tree;
  }

  /**
   * updates index of the input tree by 1
   * index 0 is for the left node
   * index 1 is for the right node
   * if option is -1, potential and joinQ need not be recomputed
   * if option is >=0, potential and joinQ should be recomputed
   * if option is > 0, the value means potential for the left sibling
   * height is only meaningful, when right tree need to be modified
   */
  void
  tgdh_update_index(KEY_TREE *tree, int index, int root_index)
  {
    if(tree == NULL) return;

    tree->tgdh_nv->index = root_index * 2 + index;

    tgdh_update_index(tree->left, 0, tree->tgdh_nv->index);
    tgdh_update_index(tree->right, 1, tree->tgdh_nv->index);
  }

  /** Updates potential and joinQ except the leaf node */
  void
  tgdh_update_potential(KEY_TREE *tree)
  {
    if(tree == NULL) return;

    tgdh_update_potential(tree->left);
    tgdh_update_potential(tree->right);

    if(tree->left != NULL){
        if((tree->left->tgdh_nv->joinQ == TRUE) &&
           (tree->right->tgdh_nv->joinQ == TRUE)){
            tree->tgdh_nv->potential =
                tree->left->tgdh_nv->potential + 1;
            tree->tgdh_nv->joinQ = TRUE;
          }
        else{
            tree->tgdh_nv->potential =
                MAX(tree->left->tgdh_nv->potential,
                    tree->right->tgdh_nv->potential);
            tree->tgdh_nv->joinQ = FALSE;
          }
      }
  }

  /** update joinQ, potential of key_path */
  void
  tgdh_update_key_path(KEY_TREE **tree)
  {
    if((*tree) != NULL){
        while((*tree)->parent != NULL){
            (*tree) = (*tree)->parent;
            if(((*tree)->left->tgdh_nv->joinQ == 0) ||
               ((*tree)->right->tgdh_nv->joinQ == 0))
              (*tree)->tgdh_nv->joinQ = FALSE;
            else (*tree)->tgdh_nv->joinQ = TRUE;
            (*tree)->tgdh_nv->potential =
                MAX((*tree)->left->tgdh_nv->potential,
                    (*tree)->right->tgdh_nv->potential);
            if(((*tree)->left->tgdh_nv->potential >= 0) &&
               ((*tree)->right->tgdh_nv->potential >= 0) &&
               ((*tree)->left->tgdh_nv->joinQ ==TRUE) &&
               ((*tree)->right->tgdh_nv->joinQ ==TRUE))
              (*tree)->tgdh_nv->potential++;
            if((*tree)->tgdh_nv->member != NULL)
              (*tree)->tgdh_nv->member = NULL;
          }
      }
  }

  /** leaderQ: true if I am the node (first or worst fit) false otherwise */
  int
  leaderQ(KEY_TREE *tree, GKA_NAME *my_name)
  {
    KEY_TREE *tmp_tree;

    tmp_tree = tree;

    while(tmp_tree->right != NULL) tmp_tree = tmp_tree->right;
    if(strcmp(tmp_tree->tgdh_nv->member->member_name, my_name) == 0)
      return TRUE;
    else return FALSE;
  }

  /**
   * copies tree structure, but to finish the real copy, we need to
   * call tgdh_dup_tree, which finishes prev and next pointer
   */
  KEY_TREE
  *tgdh_copy_tree(KEY_TREE *src)
  {
    KEY_TREE *dst=NULL;

    if(src != NULL){
        dst = (KEY_TREE *) calloc(sizeof(KEY_TREE), 1);
        if(src->tgdh_nv != NULL){
            dst->tgdh_nv = (TGDH_NV *) calloc(sizeof(TGDH_NV), 1);
            dst->tgdh_nv->index = src->tgdh_nv->index;
            dst->tgdh_nv->joinQ=src->tgdh_nv->joinQ;
            dst->tgdh_nv->potential=src->tgdh_nv->potential;
            dst->tgdh_nv->height=src->tgdh_nv->height;
            dst->tgdh_nv->num_node=src->tgdh_nv->num_node;
            if(src->tgdh_nv->key != NULL){
                dst->tgdh_nv->key = BN_dup(src->tgdh_nv->key);
              }
            if(src->tgdh_nv->bkey != NULL){
                dst->tgdh_nv->bkey = BN_dup(src->tgdh_nv->bkey);
              }
            if(src->tgdh_nv->member != NULL){
                dst->tgdh_nv->member = (TGDH_GM *) calloc(sizeof(TGDH_GM),1);
                if(src->tgdh_nv->member->member_name != NULL){
                    dst->tgdh_nv->member->member_name=(GKA_NAME *)
                        calloc(sizeof(GKA_NAME)*MAX_LGT_NAME,1);
                    strncpy (dst->tgdh_nv->member->member_name,
                             src->tgdh_nv->member->member_name,MAX_LGT_NAME);
                  }
                if(src->tgdh_nv->member->cert != NULL){
                    dst->tgdh_nv->member->cert = X509_dup(src->tgdh_nv->member->cert);
//                    PARCCertificate *wrapper = parcCertificate_CreateFromInstance(PARCX509CertificateInterface, src->tgdh_nv->member->cert);
//                    PARCBuffer *certificateDER = parcCertificate_GetDEREncodedCertificate(wrapper);
//                    dst->tgdh_nv->member->cert = parcX509Certificate_CreateFromDERBuffer(certificateDER);
                  }
              }
          }
        dst->left = tgdh_copy_tree(src->left);
        dst->right = tgdh_copy_tree(src->right);
        if(dst->left) {
            dst->left->parent = dst;
          }
        if(dst->right) {
            dst->right->parent = dst;
          }
      }

    return dst;
  }

  /**
   * finishes the copy process of one tree to another...
   * Mainly, it just handles prev and next pointer
   */
  KEY_TREE
  *tgdh_dup_tree(KEY_TREE *src)
  {
    KEY_TREE *dst=NULL;
    KEY_TREE *tmp1_src=NULL, *tmp1_dst=NULL;
    KEY_TREE *tmp2_src=NULL, *tmp2_dst=NULL;

    dst = tgdh_copy_tree(src);
    if(src != NULL){
        tmp1_src = tgdh_search_member(src, 2, NULL);
        tmp2_src = tmp1_src->next;
        tmp1_dst = tgdh_search_member(dst, 4,
                                      tmp1_src->tgdh_nv->member->member_name);
        while(tmp2_src != NULL){
            tmp2_dst = tgdh_search_member(tmp1_dst, 1, NULL);
            tmp1_dst->next = tmp2_dst;
            tmp2_dst->prev = tmp1_dst;
            tmp2_src = tmp2_src->next;
            tmp1_src = tmp1_src->next;
            tmp1_dst = tmp2_dst;
          }
      }

    return dst;
  }


  /** copies or changes tgdh_nv values of src node to dst node */
  void
  tgdh_copy_node(KEY_TREE *src, KEY_TREE *dst)
  {
    if(src->tgdh_nv != NULL){
        dst->tgdh_nv->index = src->tgdh_nv->index;
        dst->tgdh_nv->joinQ=src->tgdh_nv->joinQ;
        dst->tgdh_nv->potential=src->tgdh_nv->potential;
        dst->tgdh_nv->height=src->tgdh_nv->height;
        dst->tgdh_nv->num_node=src->tgdh_nv->num_node;
        if(src->tgdh_nv->key != NULL){
            gka_swap((void **)&(src->tgdh_nv->key),(void **)&(dst->tgdh_nv->key));
          }
        if(src->tgdh_nv->bkey != NULL){
            gka_swap((void **)&(src->tgdh_nv->bkey),(void **)&(dst->tgdh_nv->bkey));
          }
        if(src->tgdh_nv->member != NULL){
            gka_swap((void **)&(src->tgdh_nv->member), (void **)&(dst->tgdh_nv->member));
          }
      }
  }

  /* tgdh_swap_bkey swap my null bkey with meaningful bkey from new token */
  void
  tgdh_swap_bkey(KEY_TREE *src, KEY_TREE *dst)
  {
    if(src->left != NULL){
        tgdh_swap_bkey(src->left, dst->left);
        tgdh_swap_bkey(src->right, dst->right);
      }
    if(src != NULL){
        if((src->tgdh_nv->bkey != NULL) && (dst->tgdh_nv->bkey == NULL)){
            gka_swap((void **)&(src->tgdh_nv->bkey),(void **)&(dst->tgdh_nv->bkey));
          }
      }
  }

  /**
   * copies meaningful bkey from new token to my null
   * token, used for cache update
   */
  void
  tgdh_copy_bkey(KEY_TREE *src, KEY_TREE *dst)
  {
    if(src->left != NULL){
        tgdh_copy_bkey(src->left, dst->left);
        tgdh_copy_bkey(src->right, dst->right);
      }
    if(src != NULL){
        if(src->tgdh_nv->bkey != NULL){
            if(dst->tgdh_nv->bkey == NULL){
                dst->tgdh_nv->bkey = BN_dup(src->tgdh_nv->bkey);
              }
          }
      }
  }

  /**
   * checks whether new_ctx has useful information
   * If it has, return 1,
   * else, return 0
   */
  int
  tgdh_check_useful(KEY_TREE *newtree, KEY_TREE *mytree)
  {
    KEY_TREE *head_new=NULL, *tail_new=NULL;
    KEY_TREE *head_my=NULL, *tail_my=NULL;

    head_new=tail_new=newtree;
    head_my=tail_my=mytree;

    tgdh_init_bfs(newtree);
    tgdh_init_bfs(mytree);

    while(head_new != NULL){
        if(head_new->tgdh_nv->bkey!=NULL){
            if(head_my->tgdh_nv->bkey==NULL){
                return 1;
              }
            else{
                if(BN_cmp(head_new->tgdh_nv->bkey, head_my->tgdh_nv->bkey) != 0){
                    return 1;
                  }
              }
          }
        /* Queue handling */
        if(head_new->left){
            tail_new->bfs = head_new->left;
            tail_new = tail_new->bfs;
            tail_my->bfs = head_my->left;
            tail_my = tail_my->bfs;
          }
        if(head_new->right){
            tail_new->bfs = head_new->right;
            tail_new = tail_new->bfs;
            tail_my->bfs = head_my->right;
            tail_my = tail_my->bfs;
          }
        head_new = head_new->bfs;
        head_my = head_my->bfs;

      }
    tgdh_init_bfs(newtree);
    tgdh_init_bfs(mytree);

    return 0;
  }

  /** initializes bfs pointers to NULL for each node */
  void
  tgdh_init_bfs(KEY_TREE *tree)
  {
    if(tree != NULL){
        if(tree->left){
            tgdh_init_bfs(tree->left);
            tgdh_init_bfs(tree->right);
          }
        if(tree != NULL){
            tree->bfs = NULL;
          }
      }
  }
