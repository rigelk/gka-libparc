#ifndef libparc_parc_tgdh_h
#define libparc_parc_tgdh_h

/** @file icnet_utils_tgdh.h
 * @brief Function prototypes for the TGDH API.
 *
 * This contains the main functions and concepts meant
 * to be used by external libraries.
 *
 * @author Pierre-Antoine Rault (rigelk)
 * @bug No known bugs.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "openssl/bn.h"
#include "openssl/dsa.h"

#include <parc/security/parc_Security.h>
#include <parc/security/parc_X509Certificate.h>
#include <parc/security/parc_Key.h>

#include "parc_tgdh_common.h"
#include "parc_tgdh_tree.h"
#include "parc_tgdh_certs.h"

#define TGDH_API_VERSION "1.0"

typedef struct gka_token_st {
  uint length;
  gka_uchar *t_data;
} GKA_TOKEN;

/* TGDH_NAME_LIST: Linked list of users */
typedef struct tgdh_name_list {
  GKA_NAME *member_name;
  struct tgdh_name_list *next;
} TGDH_NAME_LIST;

/* TGDH_CONTEXT: BGKA context */
typedef struct tgdh_context_st {
  GKA_NAME *member_name;
  GKA_NAME *group_name;
  BIGNUM *group_secret;
  gka_uchar *group_secret_hash; /* session_key */
  KEY_TREE *root;
  KEY_TREE *cache;
  GKA_KEY *params; /* used to generate sponsor key/session random, both BIGNUM random numbers of (params->q)bits */
  GKA_KEY *pkey;
  int status;
  int merge_token;
  BIGNUM *tmp_key;
  BIGNUM *tmp_bkey;
  uint epoch;
} TGDH_CONTEXT;

/* MESSAGE TYPE definitions */
enum TGDH_MSG_TYPE
{
    TGDH_KEY_MERGE_UPDATE,
    PROCESS_EVENT,
    TGDH_INVALID
};

typedef struct TGDH_token_info {
  GKA_NAME *group_name;
  enum TGDH_MSG_TYPE message_type;
  time_t time_stamp;
  GKA_NAME *sender_name;
  /*  uint epoch; */
} TGDH_TOKEN_INFO;

/* TOKEN_LIST: Linked list of tokens */
typedef struct token_list {
  GKA_TOKEN *token;
  struct token_list *next;
  struct token_list *end;
} TOKEN_LIST;

/* TREE_LIST: Lisked list of key trees to be used in tgdh_cascade */
typedef struct tree_list {
  KEY_TREE *tree;
  struct tree_list *next;
  struct tree_list *end;
} TREE_LIST;

/* TGDH_SIGN: signature of a tree */
typedef struct tgdh_sign_st {
  gka_uchar *signature;
  uint length;
} TGDH_SIGN;

/* tgdh_sign_message: It signs the token using the current user public
 * key scheme. The signature will be appended to the begining of the
 * input token
 */
int tgdh_sign_message(
        TGDH_CONTEXT *ctx,
        GKA_TOKEN *input
        );
int tgdh_verify_sign(
        TGDH_CONTEXT *ctx,
        TGDH_CONTEXT *new_ctx,
        GKA_TOKEN *input,
        GKA_NAME *member_name,
        TGDH_SIGN *sign
        );
int tgdh_remove_sign(
        GKA_TOKEN *input,
        TGDH_SIGN **sign
        );
int tgdh_restore_sign(
        GKA_TOKEN *input,
        TGDH_SIGN **signature
        );

/** \addtogroup Public API
 * @brief public API of the protocol, as meant to be used by external programs
 * @{
 */

/**
 * @brief is called by the new member in order to create its
 * own context. Main functionality of this function is to generate
 * session random for the member
 * @param [in,out] ctx
 * @param [in]     member_name
 * @param [in]     group_name
 * @return int signal
 */
int tgdh_new_member(TGDH_CONTEXT **ctx, GKA_NAME *member_name,
                    GKA_NAME *group_name);

/* tgdh_merge_req is called by every members in both groups and only
 * the sponsors will return a output token
 *   o When any addtive event happens this function will be called.
 *   o In other words, if merge and leave happen, we need to call this
 *     function also.
 *   o If only addtive event happens, users_leaving should be NULL.
 *   ctx: context of the caller
 *   member_name: name of the caller
 *   users_leaving: name of the leaving members
 *   group_name: target group name
 *   output: output token(input token of tgdh_merge)
 */
/**
 * @brief tgdh_merge_req
 * @param ctx
 * @param member_name
 * @param group_name
 * @param users_leaving
 * @param output
 * @return int signal
 */
int tgdh_merge_req (TGDH_CONTEXT *ctx, GKA_NAME *member_name,
                    GKA_NAME *group_name, GKA_NAME *users_leaving[],
                    GKA_TOKEN **output);

/* tgdh_cascade is called by every member several times until every
 * member can compute the new group key when network faults occur.
 * Only the sponsors return output.
 *   ctx: context of the caller
 *   member_name: name of the caller
 *   member_list: list of the leaving members
 *   list: Linked list of input tokens(previous output token of
 *         tgdh_cascade or tgdh_merge_req )
 *   output: output token(will be used as next input token of tgdh_cascade)
 */
/**
 * @brief tgdh_cascade
 * @param ctx
 * @param group_name
 * @param users_leaving
 * @param list
 * @param output
 * @return int signal
 */
int tgdh_cascade(TGDH_CONTEXT **ctx, GKA_NAME *group_name,
                 GKA_NAME *users_leaving[],
                 TOKEN_LIST *list, GKA_TOKEN **output);

/* remove_member removes leaving members from the current tree.
 * o Reason for this function: It was leave part of tgdh_cascade
 *    function, but I decided to make a function since we need to add
 *    this functionality to tgdh_merge_req too...
 * o What is it doing?
 *   - This function will only remove the leaving members...
 *   - No key update happens...
 */
/**
 * @brief tgdh_remove_member
 * @param ctx
 * @param users_leaving
 * @param sponsor_list
 * @return int signal
 */
int tgdh_remove_member(TGDH_CONTEXT *ctx, GKA_NAME *users_leaving[],
                  KEY_TREE *sponsor_list[]);

/** @}*/

/* private
 * CONTEXT FUNCTIONS
 * used only by tgdh_cascade, tgdh_merge_req and tgdh_new_member
 */

/**
 * @brief Creates the tree context
 * @pre *ctx has to be NULL
 * @param ctx
 * @return int signal
 */
int tgdh_create_ctx(TGDH_CONTEXT **ctx);

/**
 * @brief Frees the space occupied by the current context
 * Including the group_members_list.
 *   if flag == 1, delete all context
 *   if flag == 0, delete all except the tree(used for merge)
 * @param ctx
 * @param flag
 */
void tgdh_destroy_ctx (TGDH_CONTEXT **ctx, int flag);

/* private
 * TOKEN FUNCTIONS
 * used only by tgdh_cascade and tgdh_merge_req
 */

/* tgdh_encode using information from the current context, it generates
 * the output token.
 */
int tgdh_encode(TGDH_CONTEXT *ctx, GKA_TOKEN **output,
                TGDH_TOKEN_INFO *info);

/* Converts tree structure to unsigned character string */
void tgdh_map_encode(gka_uchar *stream, uint *pos, KEY_TREE *root);

/* tgdh_decode using information from the input token, it creates
 * ctx. info is also created here. It contains data recovered from
 * input such as message_type, sender, etc. (See structure for more
 * details) in readable format.
 */
int tgdh_decode(TGDH_CONTEXT **ctx, GKA_TOKEN *input,
                TGDH_TOKEN_INFO **info);

/* tgdh_map_decode decode input token to generate tree for the new
 * tree
 * *tree should be pointer to the root node
 */
int tgdh_map_decode(const GKA_TOKEN *input, uint *pos,
                    TGDH_CONTEXT **ctx);
/* tgdh_create_token_info: It creates the info token. */
int tgdh_create_token_info (TGDH_TOKEN_INFO **info, GKA_NAME const *group,
                            enum TGDH_MSG_TYPE msg_type, time_t time,
                            GKA_NAME *sender/*, uint epoch*/);
/* tgdh_destroy_token_info: It frees the memory of the token. */
void tgdh_destroy_token (GKA_TOKEN **token);
/* tgdh_destroy_token_info: It frees the memory of the token. */
void tgdh_destroy_token_info (TGDH_TOKEN_INFO **info);

/* private
 * SPONSOR FUNCTIONS
 * used only by tgdh_cascade and tgdh_remove_member
 */

/* Find all sponsors */
int find_sponsors(KEY_TREE *root, KEY_TREE *sponsor_list[]);
/* remove_sponsor: remove the sponsor from the sponsor list */
int remove_sponsor(GKA_NAME *sponsor_list[], GKA_NAME *sponsor);

/* private
 * TREE LIST HELPERS
 * used only by tgdh_cascade
 */

/* Make a tree list for merge */
TREE_LIST *add_tree_list(TREE_LIST *list, KEY_TREE *tree);
/* Remove all tree list */
void remove_tree_list(TREE_LIST **list);

/* private
 * CRYPTO FUNCTIONS
 */

/** TODO: rewrite
 * @brief gka_get_dsa_key to get the DSA key depending on the type
 * @param member_name
 * @param type GKA_KEY_TYPE (either public, private or parameters of the DSA)
 * @return a key of any type
 */
DSA *gka_get_dsa_key (char *member_name,
                      enum GKA_KEY_TYPE type);
DSA *gka_get_key (char *member_name,
                  enum GKA_KEY_TYPE type);

/** TODO: rewrite without DSA
 * @brief _gka_get_pkey to get private key
 * The initial goal of this function was to load the private key from disk
 *
 * @param member_name (if NULL then DSA parameters will be read from the disk)
 * @return private key or NULL
 */
EVP_PKEY *_gka_get_pkey (char *member_name);

/** TODO: rewrite without DSA
 * @brief gka_get_dsa_param
 * @return dsa parameter or NULL
 */
DSA *gka_get_dsa_param();

/** @file
 * @brief gka_read_DSA reads a DSA structure from disk depending on
 * GKA_KEY_TYPE (GKA_PARAMS, GKA_PRIV, GKA_PUB)
 * @returns the structure if succeed otherwise NULL is returned.
 *
 * used by tgdh_new_member only
 * uses gka_get_dsa_key
 */
DSA *gka_read_dsa(char *member_name, enum GKA_KEY_TYPE type);

/** @file
 * @brief tgdh_compute_bkey Computes and returns bkey
 * @return bkey
 *
 * used by tgdh_new_member, tgdh_cascade and tgdh_merge_req only
 */
BIGNUM *tgdh_compute_bkey (BIGNUM *key, GKA_KEY *params);

/** @file
 * @brief tgdh_compare_key
 * @param ctx
 * @param num
 * @return int boolean
 */
int tgdh_compare_key(TGDH_CONTEXT *ctx[], int num);

/** @file
 * @brief check_group_secret
 * @param ctx
 * @param num_users
 */
int tgdh_check_group_secret (TGDH_CONTEXT *ctx[], int num_users);

/** @file
 * @brief tgdh_rand Generates a new random number of "params->q" bits, using
 * the default parameters.
 * @return pointer to a dsa structure where the random value
 *         resides.
 *         NULL if an error occurs.
 *
 * used by tgdh_new_member, tgdh_cascade and tgdh_merge_req only
 */
BIGNUM *tgdh_rand (GKA_KEY *param);

/** @file
 * @brief tgdh_compute_secret_hash computes the hash of the group_secret.
 * @pre ctx->group_secret has to be valid.
 *
 * used by tgdh_new_member, tgdh_cascade only
 */
int tgdh_compute_secret_hash (TGDH_CONTEXT *ctx);

/* int_encode: It puts an integer number in stream. Note that the size
 * of the integer number is addded to the stream as well.
 */
/* NOTE: HTONL should be added here */
void int_encode(gka_uchar *stream, gka_uint *pos, gka_uint data);


/* int_decode: It gets an integer number from input->t_data. Note that
 * the size of the integer number is decoded first, and then the
 * actual number is decoded.
 * Returns: 1 succeed.
 *          0 Fails.
 */
int int_decode(const GKA_TOKEN *input, gka_uint *pos, gka_uint *data);

/* string_encode: It puts the valid 'c' string into stream. It first
 * stores the message length (including \0) and the the actual
 * message.
 */
void string_encode (gka_uchar *stream, gka_uint *pos, char *data);

/* string_decode: It restores a valid 'c' string from
 * input->t_data. First the string length is decode (this one should
 * have \0 already), and the actual string.
 * Returns: 1 succeed.
 *          0 Fails.
 */
int string_decode (const GKA_TOKEN *input, gka_uint *pos, char *data);

/* bn_encode: BIGNUM encoding. */
void bn_encode (gka_uchar *stream, gka_uint *pos, BIGNUM *num);

/* bn_decode: BIGNUM decoding.
 * Preconditions: num has to be different from NULL.
 * Returns: 1 succeed.
 *          0 Fails.
 */
int bn_decode (const GKA_TOKEN *input, gka_uint *pos, BIGNUM *num);

#endif // libparc_parc_tgdh_h
