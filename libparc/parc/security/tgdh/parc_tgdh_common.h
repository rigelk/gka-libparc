#ifndef libparc_parc_tgdh_common_h
#define libparc_parc_tgdh_common_h

/** @file icnet_utils_tgdh_common.h
 * @brief Common values and parameters set in stone for TGDH.
 * @author Pierre-Antoine Rault (rigelk)
 */

  /* parameters */
#define NUM_USERS 256
#define MSG_SIZE 65536
#define INT_SIZE sizeof(int)
#define LENGTH_SIZE 4 /* The length of the size in a token */
#define TOTAL_INT INT_SIZE+LENGTH_SIZE

#define MAX_LIST 200 /* Maximum number of members */
#define MAX_LGT_NAME 50 /* Maximum length a GKA_NAME can have. */

  /* gka_get_cert stuff */
#define PARAM_CERT    "dsa_param.pem"
#define PUB_CERT      "cert"
#define CA_CERT       "cacert.pem"

  /* gka_read_key stuff */
#define COMMON_FILE   "public_values.gka"
#define PUB_FMT       "pub"
#define PRV_FMT       "priv"
#ifdef USE_GKA_READ_DSA
#define FILE_EXT "gka"
#else
#define FILE_EXT      "pem"
#endif

  /* name of the environment variable that specifies the path to users' certificates  */
#define CERT_PATH     "GKA_PARAM"
#define PARAM_PATH    "GKA_PRIVKEY"
#define CA_PATH       "GKA_CA"

  /* macro shortcuts */
#define OK 1

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef MAX
#define MAX(a,b) \
  ({ __typeof__ (a) _a = (a); \
  __typeof__ (b) _b = (b); \
  _a > _b ? _a : _b; })
#endif

#ifndef MIN
#define MIN(a,b) \
  ({ __typeof__ (a) _a = (a); \
  __typeof__ (b) _b = (b); \
  _a < _b ? _a : _b; })
#endif

/* data structures */
typedef char GKA_NAME;
typedef unsigned char gka_uchar;
typedef unsigned int gka_uint;

/* GKA_KEY_TYPE definitions, used by gka_read_dsa */
enum GKA_KEY_TYPE { GKA_PARAMS,
                    GKA_PRV,
                    GKA_PUB };

#endif // libparc_parc_tgdh_common_h
