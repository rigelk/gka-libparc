/*
 * Copyright (c) 2017 Cisco and/or its affiliates.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <config.h>
#include <stdio.h>
#include <LongBow/unit-test.h>

#include <parc/algol/parc_SafeMemory.h>
#include <parc/security/parc_Security.h>
#include "../parc_tgdh.h"
#include "../parc_tgdh_common.h"
#include "../parc_tgdh_errors.h"
#include "../parc_tgdh_catch.h"

/* test settings */
#define GROUP_NAME  "cicn"
#define NAME_LENGTH 10
#define E4C_THREADSAFE

/* TGDH_LIST: list of users */
typedef struct tgdh_list {
  int num;
  int status;
  GKA_NAME *list[NUM_USERS+1];
  GKA_NAME *leaving_list[NUM_USERS+1];
  TOKEN_LIST *token_list;
#ifdef TEST_TIMING
  double tmp_time;
  unsigned max_round;
  unsigned average_round;
#endif
  struct tgdh_list *next;
} TGDH_LIST;

TOKEN_LIST *remove_token(TOKEN_LIST **list_token)
{
  TOKEN_LIST *tmp_token;

  if((*list_token) == NULL) return NULL;
  tgdh_destroy_token(&((*list_token)->token));
  (*list_token)->token=NULL;
  if((*list_token)->next){
      tmp_token = (*list_token)->next;
      tmp_token->end = (*list_token)->end;
    }
  else{
      tmp_token = NULL;
    }

  if((*list_token) != NULL) free((*list_token));
  (*list_token) = NULL;

  return tmp_token;
}

TOKEN_LIST *remove_all_token(TOKEN_LIST *list_token)
{
  while(list_token != NULL){
      list_token=remove_token(&list_token);
    }
  return NULL;
}

TOKEN_LIST *add_token(TOKEN_LIST *list_token, GKA_TOKEN *token)
{
  TOKEN_LIST *tmp_list=NULL;

  if(token == NULL) return NULL;

  if(list_token == NULL){
      list_token = (TOKEN_LIST *) calloc(sizeof(TOKEN_LIST), 1);
      if(list_token == NULL){
          return NULL;
        }
      list_token->token = token;
      list_token->end = list_token;
      list_token->next = NULL;
    }
  else{
      if(list_token->end != list_token){
          if(list_token->next == NULL){
              remove_all_token(list_token);
              return NULL;
            }
        }
      else{
          if(list_token->end->next != NULL){
              remove_all_token(list_token);
              return NULL;
            }
        }
      tmp_list = (TOKEN_LIST *) calloc(sizeof(TOKEN_LIST), 1);
      tmp_list->token = token;
      list_token->end->next = tmp_list;
      list_token->end = tmp_list;
      tmp_list->next = NULL;
      tmp_list->end = NULL;
    }

  return list_token;
}

void destroy_list(TGDH_LIST **list)
{
  TGDH_LIST *tmp_list=NULL;

  tmp_list = (*list)->next;
  while((*list) != NULL){
      if((*list)->token_list != NULL){
          (*list)->token_list = remove_all_token((*list)->token_list);
        }
      free(*list);
      (*list) = tmp_list;
      if((*list) == NULL){
          break;
        }
      else{
          tmp_list = (*list)->next;
        }
    }
}

LONGBOW_TEST_RUNNER(parc_tgdh)
{
    LONGBOW_RUN_TEST_FIXTURE(Global);
}

LONGBOW_TEST_RUNNER_SETUP(parc_tgdh)
{
    return LONGBOW_STATUS_SUCCEEDED;
}

// The Test Runner calls this function once after all the Test Fixtures are run.
LONGBOW_TEST_RUNNER_TEARDOWN(parc_tgdh)
{
    return LONGBOW_STATUS_SUCCEEDED;
}

LONGBOW_TEST_FIXTURE(Global)
{
    LONGBOW_RUN_TEST_CASE(Global, parc_tgdh);
}

LONGBOW_TEST_FIXTURE_SETUP(Global)
{
    parcMemory_SetInterface(&PARCSafeMemoryAsPARCMemory);
    parcSecurity_Init();
    return LONGBOW_STATUS_SUCCEEDED;
}

LONGBOW_TEST_FIXTURE_TEARDOWN(Global)
{
    parcSecurity_Fini();
    if (parcSafeMemory_ReportAllocation(STDOUT_FILENO) != 0) {
        printf("('%s' leaks memory by %d (allocs - frees)) ", longBowTestCase_GetName(testCase), parcMemory_Outstanding());
        return LONGBOW_STATUS_MEMORYLEAK;
    }
    return LONGBOW_STATUS_SUCCEEDED;
}

LONGBOW_TEST_CASE(Global, parc_tgdh)
{
    #define NUM_USERS 3

    GKA_NAME *user[NUM_USERS+1] = { NULL };

    TGDH_LIST *list=NULL, *init_list=NULL, *tmp_list=NULL;

    TGDH_CONTEXT *ctx[NUM_USERS];
    GKA_TOKEN *tmp = NULL;
    TOKEN_LIST *input = NULL;
    GKA_TOKEN *output[NUM_USERS+1];
    int num_users=NUM_USERS;
    char *group_name=(char*)GROUP_NAME;
    int leaving_member=0;

    int ret=OK, Ret=CONTINUE;
    int i=0, j=0, k=0;
    int num_round=0;
    TOKEN_LIST *curr_token = NULL;
    TOKEN_LIST *tmp_input = NULL;

    e4c_context_begin(E4C_TRUE);

    tmp_input = (TOKEN_LIST *)calloc(sizeof(TOKEN_LIST), 1);
    /* Intializing the members who will join the group */
    for(i=0; i<num_users; i++){
        user[i] = NULL;
        ctx[i] = NULL;
      }
    for(i=0; i<num_users; i++){
        user[i] = (GKA_NAME *)malloc(sizeof(GKA_NAME)*MAX_LGT_NAME);
        sprintf (user[i],"%03d",i);
      }

    TRY
    {
      /* First user joining group */
      try
      {
        ret = tgdh_new_member(&ctx[0], user[0], group_name);
        DEBUG_PRINT ("tgdh_new_member returns %i \n", ret);
      }
      catch(BadPointerException)
      {
        printf("Problem in tgdh_new_member: ");
        THROW (CTX_ERROR);
      }

      /* Merging other users */
      for(i=0; i<num_users; i++) {
          output[i] = NULL;
        }

      try
      {
        for(i=1; i<num_users; i++){
            printf("BEGIN MERGES, iteration n°%d\n", i);

            ret=tgdh_new_member(&ctx[i], user[i], group_name);
            DEBUG_PRINT("\t:tgdh_new_member by %03d returns %d (should be 1 for new_member to be successful)\n", i, ret);
            if (ret!=1) THROW (CTX_ERROR);

            /* New member sends merge request */
            for(j=0; j<i; j++){
                printf("MEMBER %03d\n", j);
                ret=tgdh_merge_req(ctx[j], user[j], group_name, NULL, &output[0]);
                /*printf ("\t::tgdh_merge_req by %03d returns %d with output %u\n",
                        j,ret,(uint)output[0]->t_data);*/
                if (ret!=1) THROW (CTX_ERROR);
                if(output[0] != NULL){
                    input = add_token(input, output[0]);
                  }
                output[0]=NULL;
                DEBUG_PRINT("new member merged\n");
              }
            if(input == NULL) {
                THROW (CTX_ERROR);
              }

            /* Last member in the current group is the sponsor */
            ret=tgdh_merge_req(ctx[i], user[i], group_name, NULL, &output[1]);
            DEBUG_PRINT("\t::tgdh_merge_req by %03d returns %d with output %u\n",
                    i,ret,(uint)*output[1]->t_data);
            if (ret!=1) THROW (CTX_ERROR);
            if(output[1] == NULL){
                fprintf(stderr, "something wrong!\n\n");
              }

            input = add_token(input, output[1]);
            if(input == NULL) {
                THROW (CTX_ERROR);
              }
            output[1] = NULL;

            Ret=2;

            while(Ret != 1){
                k = 0;
                curr_token = input;

                Ret = 1;
                for(j=0; j<=i; j++){
                    DEBUG_PRINT("beginning cascade\n");
                    ret = tgdh_cascade(&ctx[j], group_name, NULL,
                                       curr_token, &tmp);
                    DEBUG_PRINT("\t::::tgdh_cascade of %03d returns %d with output %p\n",
                           j,ret, tmp);
                    if(ret <= 0) THROW (CTX_ERROR);
                    Ret = MAX(Ret, ret);
                    if(tmp != NULL){
                        if(output[k] != NULL){
                            tgdh_destroy_token(&output[k]);
                          }
                        output[k] = tmp;
                        if(output[k] == NULL) {
                            THROW (CTX_ERROR);
                          }
                        k++;
                      }
                    tmp=NULL;
                  }
                input = remove_all_token(input);

                for(j=0; j<k; j++){
                    input = add_token(input, output[j]);
                    output[j] = NULL;
                  }
              }
            input=remove_all_token(input);
            tgdh_check_group_secret(ctx, i+1);
            printf("We survived the horde n°%d!\n",i);
          }
        printf ("----------End of Join Event\n");
        assert(tgdh_check_group_secret(ctx, num_users) == OK);
        tgdh_compare_key(ctx, num_users);
      }
      catch(BadPointerException)
      {
        printf("A pointer exception has been raised.");
      }
    } // end of try block
    CATCH (CTX_ERROR)
    {
      printf("ctx error!\n");
    }
    FINALLY
    {
      for (i=0; i < NUM_USERS; i++) {
          if (ctx[i] != NULL) tgdh_destroy_ctx(&(ctx[i]), 1);
          if (user[i] != NULL) free(user[i]);
        }
      if(list != NULL) destroy_list(&list);
    }
    ETRY;

    e4c_context_end();
}

int
main(int argc, char *argv[argc])
{
    LongBowRunner *testRunner = LONGBOW_TEST_RUNNER_CREATE(parc_tgdh);
    int exitStatus = LONGBOW_TEST_MAIN(argc, argv, testRunner);
    longBowTestRunner_Destroy(&testRunner);
    exit(exitStatus);
}
