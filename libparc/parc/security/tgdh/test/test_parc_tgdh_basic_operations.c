/*
 * Copyright (c) 2017 Cisco and/or its affiliates.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <config.h>
#include <stdio.h>
#include <time.h>
#include <openssl/bio.h>
#include <LongBow/unit-test.h>

#include <parc/algol/parc_SafeMemory.h>
#include <parc/security/parc_Security.h>
#include "../parc_tgdh_certs.h"
#include "../parc_tgdh_catch.h"
#include "../parc_tgdh.h"

#include <stdint.h>

#ifdef _MSC_VER
#include <intrin.h>
#else
#include <x86intrin.h>
#endif

#define rounds 256

LONGBOW_TEST_RUNNER(parc_tgdh_basic)
{
    LONGBOW_RUN_TEST_FIXTURE(Global);
}

LONGBOW_TEST_RUNNER_SETUP(parc_tgdh_basic)
{
    return LONGBOW_STATUS_SUCCEEDED;
}

// The Test Runner calls this function once after all the Test Fixtures are run.
LONGBOW_TEST_RUNNER_TEARDOWN(parc_tgdh_basic)
{
    return LONGBOW_STATUS_SUCCEEDED;
}

LONGBOW_TEST_FIXTURE(Global)
{
    LONGBOW_RUN_TEST_CASE(Global, parc_tgdh_basic_blinding);
}

LONGBOW_TEST_FIXTURE_SETUP(Global)
{
    parcMemory_SetInterface(&PARCSafeMemoryAsPARCMemory);
    parcSecurity_Init();

    return LONGBOW_STATUS_SUCCEEDED;
}

LONGBOW_TEST_FIXTURE_TEARDOWN(Global)
{
    parcSecurity_Fini();
    if (parcSafeMemory_ReportAllocation(STDOUT_FILENO) != 0) {
        printf("('%s' leaks memory by %d (allocs - frees)) ", longBowTestCase_GetName(testCase), parcMemory_Outstanding());
        return LONGBOW_STATUS_MEMORYLEAK;
    }
    return LONGBOW_STATUS_SUCCEEDED;
}

LONGBOW_TEST_CASE(Global, parc_tgdh_basic_blinding)
{
    // optional wrapper if you don't want to just use __rdtsc() everywhere
    inline
    unsigned long long rdtsc() {
      _mm_lfence();  // optionally wait for earlier insns to retire before reading the clock
      return __rdtsc();
      _mm_lfence();  // optionally block later instructions until rdtsc retires
    }

    GKA_KEY key;
    e4c_context_begin(E4C_TRUE);
    _gka_get_local(&key, NULL, NULL, GKA_PARAMS);
    e4c_context_end();

    // we deal with DSA
    char q_str[] = "BA85C06AFFDBF45943F0E5912D9B935645CC07346B386680E4C03EC3445D7BFC99A1A3884A6423D8D86980B328208BBAE6F8BF5515F01D341FBC4F93DB222F4BF208E585B3A692BAD47917059520D6D24CCB75AA7C23FA5BD98B37B83D04A3E231AF28322FA3CD9CA36D5EA7F130716834DFF47CCBFD3E83555D35A8D3A243755CCFCC257FB2D981B988DBBE13CF3E263F3F96B76245A8F72D245880F2FD800CFD9F35069126C4148328921092E51A3686B6C17391F6C1B4BBC5A31F9D125984EA035FFB96134BD27D4A7F2F8A5F1DA1B40CF4FBC55D95845034B78C52C71F33938A40D66D70281979C671ABD1B093AF3A5A92169580C3CE9E5AEA00096AC5A0CD52C16EE3E86AD5685F51CB66F9C2D5667729A4C898FB0F2AA31DB15B48408BC2AB74A002F699357EEA1E2EC8B672D48E04D939F7DE5F58E7C177AB262811F23CC78148C62B490F463BD430E5D4A32960C1937821C15F669BE86A82A280235D26CBE5433AC297C3CEF6D7615510F101FE7E37A9EF03DF80D72991A7EFC5601E119AD66E728DB9EEF6FE164DA62A3F9905DDC137162267E62581F5D93368B5F5DCFE12BBF2951C86330FDBF1CA7F4FA518034C7DBAB26D7232C975A7ED063CC208603707B329383DAC1E2A15C7050E8F0CBF757652CE57812109A24B220BB6BFABA5A773C836B2769C1DC5E286AA24347175B879E246B85A14277DAC1D0E9BEF";
    BIGNUM *q = BN_new();
    BN_dec2bn(&q, q_str);

    uint64_t rcc = rdtsc();
    uint64_t measurement_rcc = rdtsc() - rcc;

    printf("Time needed for the measure itself: %" PRIu64 " reference CPU cycles\n", measurement_rcc);

    // clocking one operation

    rcc = rdtsc();
    tgdh_compute_bkey(q, &key);
    uint64_t rcc_for_blinding = rdtsc() - rcc - measurement_rcc;

    printf("Time needed for the blinding operation: %" PRIu64 " reference CPU cycles", rcc_for_blinding);

    clock_t time = clock();
    tgdh_compute_bkey(q, &key);
    clock_t time_for_blinding = clock() - time;

    printf(" (%Lf seconds)", (long double)time_for_blinding / (long double)CLOCKS_PER_SEC);
    printf("\n");

    // average result for TGDH: log2(n) operations

    double l = 3 * log(rounds) / log(2.);
    printf("Time needed for O log2(%i) blinding operations: %" PRIu64 " reference CPU cycles", rounds, (int)l*rcc_for_blinding);
    printf(" (%Lf seconds)", (long double)time_for_blinding / (long double)CLOCKS_PER_SEC * (long double)l);
    printf("\n");

    // clocking multiple operations

    rcc = rdtsc();
    int i = rounds;
    while (--i > 0) {
      tgdh_compute_bkey(q, &key);
    }
    rcc_for_blinding = rdtsc() - rcc - measurement_rcc;

    printf("Time needed for %i blinding operations: %" PRIu64 " reference CPU cycles", rounds, rcc_for_blinding);

    time = clock();
    i = rounds;
    while (--i > 0) {
      tgdh_compute_bkey(q, &key);
    }
    time_for_blinding = clock() - time;

    printf(" (%Lf seconds)", (long double)time_for_blinding / (long double)CLOCKS_PER_SEC);
    printf("\n");

    BN_free(key.params.nid_params.dsa.param_dsa_q);
    BN_free(key.params.nid_params.dsa.param_dsa_p);
    BN_free(key.params.nid_params.dsa.param_dsa_g);
    BN_free(q);
}

int
main(int argc, char *argv[argc])
{
    LongBowRunner *testRunner = LONGBOW_TEST_RUNNER_CREATE(parc_tgdh_basic);
    int exitStatus = LONGBOW_TEST_MAIN(argc, argv, testRunner);
    longBowTestRunner_Destroy(&testRunner);
    exit(exitStatus);
}
