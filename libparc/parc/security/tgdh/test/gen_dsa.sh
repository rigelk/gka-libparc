#!/bin/bash

usage ()
{
  echo "usage: number_of_accounts"
  exit;
}

if [ "$#" != "1" ]
then
  usage
fi

#echo "generating params"
#openssl dsaparam -genkey 1024 -out dsaparam.pem

for f in $( seq -f "%03g" 0 1 "$1" ); do
  priv="${f}_dsa_key.pem"
  cert="${f}_dsa_cert.pem"
  p12="${f}_dsa.p12"
  #openssl pkcs12 -export -out "`basename $f .pem`.p12" -inkey "$f" -in "$f" -certfile ./myca.pem -password pass:$2;

  # generate both key and DSA parameters (both will be stored in dsakey.pem)
  openssl gendsa -out "$priv" dsaparam.pem
  openssl req -x509 -new -days 365 \
    -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com" -nodes \
    -key "$priv" -out "$cert"

  # print private and public key with DSA params
  #openssl dsa -in dsakey.pem -text -noout

  # print certificate
  #openssl x509 -in dsacert.pem -text -noout

  # print only DSA params from key file
  #openssl dsaparam -in dsakey.pem -text -noout

  # generate container
  openssl pkcs12 -export -inkey "$priv" -in "$cert" -out "$p12" -passout pass:blueberry
done

cp "000_dsa.p12" "test_dsa.p12"

exit 0

