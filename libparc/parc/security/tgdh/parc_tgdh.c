#include "parc_tgdh_errors.h"
#include "parc_tgdh_catch.h"
#include "parc_tgdh_compat.h" /* Preparing for OpenSSL 1.1.0+ */
#include "parc_tgdh_certs.h"
#include "parc_tgdh.h"

#include "assert.h"
#include <netinet/in.h> /* Needed by htonl and ntohl */
#include <math.h>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <openssl/rsa.h>
#include <openssl/dsa.h>
#include <openssl/bio.h>
#include <openssl/err.h>

/* dmalloc CNR.  */
#ifdef USE_DMALLOC
#include <dmalloc.h>
#endif

#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

  /* tgdh_new_member is called by the new member in order to create its
   * own context. Main functionality of this function is to generate
   * session random for the member
   */
  int tgdh_new_member(TGDH_CONTEXT **ctx, GKA_NAME *member_name,
                      GKA_NAME *group_name)
  {
    int ret=OK;

    if (member_name == NULL)
      return INVALID_MEMBER_NAME;
    if ((strlen(member_name) == 0) ||
        (strlen(member_name) > MAX_LGT_NAME))
      return INVALID_LGT_NAME;
    if (group_name == NULL)
      return INVALID_GROUP_NAME;
    if ((strlen(group_name) == 0) ||
        (strlen(group_name) > MAX_LGT_NAME))
      return INVALID_LGT_NAME;

    TRY
    {
      try{
        ret=tgdh_create_ctx(ctx);
      }catch(BadPointerException){
        puts("bad pointer error detected");
        printf("tgdh_create_ctx returned %i ; ",ret);
        ret = CTX_ERROR;
      }finally{
        if (ret != OK) {THROW_;}
      }

      (*ctx)->member_name=(GKA_NAME *) calloc(sizeof(GKA_NAME)*MAX_LGT_NAME,1);
      if (((*ctx)->member_name) == NULL) { ret=MALLOC_ERROR; THROW_; }
      strncpy((*ctx)->member_name,member_name,MAX_LGT_NAME);
      (*ctx)->group_name=(GKA_NAME *) calloc(sizeof(GKA_NAME)*MAX_LGT_NAME,1);
      if (((*ctx)->group_name) == NULL) { ret=MALLOC_ERROR; THROW_; }
      strncpy((*ctx)->group_name,group_name,MAX_LGT_NAME);

      try{
      /* Get DSA parameters */
      gka_get((*ctx)->params, NULL, GKA_PARAMS);
      if ((*ctx)->params == (GKA_KEY *)NULL) { ret=INVALID_DSA_PARAMS; THROW_; }
      /* Get user private and public keys */
      gka_get((*ctx)->pkey, member_name, GKA_PRV);
      if (((*ctx)->pkey->private_key) == (EVP_PKEY*) NULL) { ret=INVALID_PRIVKEY; THROW_; }
      }catch(BadPointerException){
        puts("yet another bad pointer exception");
        printf("error getting parameters");
      }

      (*ctx)->root->tgdh_nv=(TGDH_NV *) calloc(sizeof(TGDH_NV),1);
      (*ctx)->root->tgdh_nv->member = (TGDH_GM *) calloc(sizeof(TGDH_GM),1);
      (*ctx)->root->tgdh_nv->member->member_name=(GKA_NAME *)
          calloc(sizeof(GKA_NAME)*MAX_LGT_NAME,1);
      strncpy ((*ctx)->root->tgdh_nv->member->member_name,
               (*ctx)->member_name,MAX_LGT_NAME);
      (*ctx)->root->tgdh_nv->member->cert = (X509 *) calloc(sizeof(X509),1);

      (*ctx)->root->tgdh_nv->index=1;
      (*ctx)->root->tgdh_nv->num_node=1;
      (*ctx)->root->tgdh_nv->height=0;
      (*ctx)->root->tgdh_nv->potential=-1;
      (*ctx)->root->tgdh_nv->joinQ=FALSE;

      /* I'm only member in my group... So the key is same as my session random */
      (*ctx)->root->tgdh_nv->key=tgdh_rand((*ctx)->params);
      if (BN_is_zero((*ctx)->root->tgdh_nv->key) ||
          (*ctx)->root->tgdh_nv->key==NULL){
          ret=MALLOC_ERROR;
          THROW_;
        }
      /* group_secret is same as key */
      if((*ctx)->group_secret == NULL){
          (*ctx)->group_secret=BN_dup((*ctx)->root->tgdh_nv->key);
          if ((*ctx)->group_secret == (BIGNUM *) NULL) {
              ret=MALLOC_ERROR;
              THROW_;
            }
        }
      else{
          BN_copy((*ctx)->group_secret,(*ctx)->root->tgdh_nv->key);
        }

      ret=tgdh_compute_secret_hash ((*ctx));
      if (ret!=OK) THROW_;
      (*ctx)->root->tgdh_nv->member->cert=NULL;

      /* Compute blinded Key */
      (*ctx)->root->tgdh_nv->bkey=
          tgdh_compute_bkey((*ctx)->root->tgdh_nv->key, (*ctx)->params);
      if((*ctx)->root->tgdh_nv->bkey== NULL){
          ret=MALLOC_ERROR;
          THROW_;
        }
      (*ctx)->tmp_key = (*ctx)->tmp_bkey = NULL;
      (*ctx)->status = OK;
    }
    FINALLY
    {
      if (ret!=OK) tgdh_destroy_ctx(&(*ctx),1);
    }
    ETRY;

    return ret;
  }

  /* tgdh_merge_req is called by every members in both groups and only
   * the sponsors will return a output token
   *   o When any addtive event happens this function will be called.
   *   o In other words, if merge and leave happen, we need to call this
   *     function also.
   *   o If only addtive event happens, users_leaving should be NULL.
   *   ctx: context of the caller
   *   member_name: name of the caller
   *   users_leaving: name of the leaving members
   *   group_name: target group name
   *   output: output token(input token of tgdh_merge)
   */
  int tgdh_merge_req (TGDH_CONTEXT *ctx, GKA_NAME *member_name,
                      GKA_NAME *group_name, GKA_NAME *users_leaving[],
                      GKA_TOKEN **output)
  {
    int ret=OK;
    TGDH_TOKEN_INFO *info=NULL;
    KEY_TREE *tmp_tree=NULL, *tmp1_tree=NULL;

    KEY_TREE *the_sponsor=NULL;
    int sponsor = 0;
    BN_CTX *bn_ctx=BN_CTX_new();

    TRY
    {
      if (ctx == NULL)
        { ret=CTX_ERROR; THROW_; }
      if (member_name == NULL)
        { ret=INVALID_MEMBER_NAME; THROW_; }
      if ((strlen(member_name) == 0)
          || (strlen(member_name) > MAX_LGT_NAME))
        { ret=INVALID_LGT_NAME; THROW_; }
      if (group_name == NULL)
        { ret=INVALID_GROUP_NAME; THROW_; }
      if ((strlen(group_name) == 0)
          || (strlen(group_name) > MAX_LGT_NAME))
        { ret=INVALID_LGT_NAME; THROW_; }

      /* If we want change key and if I am not the root node */
      tmp_tree = tgdh_search_member(ctx->root, 4, ctx->member_name);

      if((the_sponsor == NULL) && (tmp_tree != ctx->root) ){
          the_sponsor = tgdh_search_member(ctx->root, 6, NULL);
          if(the_sponsor == NULL){
              fprintf(stderr, "The sponsor is NULL!\n");
              ret = STRUCTURE_ERROR;
              THROW_;
            }
          else{
  #ifdef DEBUG_YD
              fprintf(stderr, "The sponsor is %s\n",
                      the_sponsor->tgdh_nv->member->member_name);
  #endif
            }
        }

      if (the_sponsor == NULL){
          /*
           * This means that I am the only member in my tree. So we don't
           * need to change key... We just need to broadcast, since I am the
           * sponsor
           */
          sponsor = 1;
          ret = OK;
          THROW_;
        }

      /* Remove keys and bkeys related with the sponsor */
      tmp_tree = the_sponsor;
      while(tmp_tree != NULL){
          if(tmp_tree->tgdh_nv->bkey != NULL){
              BN_clear_free(tmp_tree->tgdh_nv->bkey);
              tmp_tree->tgdh_nv->bkey = NULL;
            }
          if(tmp_tree->tgdh_nv->key != NULL){
              BN_clear_free(tmp_tree->tgdh_nv->key);
              tmp_tree->tgdh_nv->key = NULL;
            }
          tmp_tree = tmp_tree->parent;
        }

      /* If I am not the sponsor, quietly go out */
      if(strcmp(the_sponsor->tgdh_nv->member->member_name,
                ctx->member_name)!=0){
          ret = OK;
          THROW_;
        }

      /* Now, I am the sponsor */

      /* Generate new key and bkeys for the sponsor */
      the_sponsor->tgdh_nv->key=tgdh_rand(ctx->params);
      the_sponsor->tgdh_nv->bkey=
          tgdh_compute_bkey(the_sponsor->tgdh_nv->key, ctx->params);
      sponsor = 1;

      /* Now compute every key and bkey */
      tmp_tree = the_sponsor;

      if (tmp_tree->parent->tgdh_nv->key != NULL) {
          fprintf(stderr, "Parent key is not null 1!\n");
          ret = STRUCTURE_ERROR;
          THROW_;
        }
      if (tmp_tree->tgdh_nv->index % 2) {
          tmp1_tree = tmp_tree->parent->left;
        }
      else
        {
          tmp1_tree = tmp_tree->parent->right;
        }
      while (tmp1_tree->tgdh_nv->bkey != NULL)
        {
          /* Compute intermediate keys until I can */
          if (tmp_tree->parent->tgdh_nv->key != NULL) {
              fprintf(stderr, "Parent key is not null 2!\n");
              ret=STRUCTURE_ERROR;
              THROW_;
            }
          tmp_tree->parent->tgdh_nv->key = BN_new();
          sponsor = 1;
//          const BIGNUM *ctx_param_p, *ctx_param_q, *ctx_param_g;
//          DSA_get0_pqg(ctx->params->param, &ctx_param_p, &ctx_param_q, &ctx_param_g);
          if (tmp_tree->parent->left->tgdh_nv->key != NULL) {
              ret = BN_mod(tmp_tree->parent->left->tgdh_nv->key,
                           tmp_tree->parent->left->tgdh_nv->key,
                           (const BIGNUM*)ctx->params->params.nid_params.dsa.param_dsa_q, bn_ctx);
              if(ret != OK) THROW_;

              ret=BN_mod_exp(tmp_tree->parent->tgdh_nv->key,
                             tmp_tree->parent->right->tgdh_nv->bkey,
                             tmp_tree->parent->left->tgdh_nv->key,
                             (const BIGNUM*)ctx->params->params.nid_params.dsa.param_dsa_q,bn_ctx);
            }
          else
            {
              ret = BN_mod(tmp_tree->parent->right->tgdh_nv->key,
                           tmp_tree->parent->right->tgdh_nv->key,
                           (const BIGNUM*)ctx->params->params.nid_params.dsa.param_dsa_q, bn_ctx);
              if(ret != OK) THROW_;
              ret=BN_mod_exp(tmp_tree->parent->tgdh_nv->key,
                             tmp_tree->parent->left->tgdh_nv->bkey,
                             tmp_tree->parent->right->tgdh_nv->key,
                             (const BIGNUM*)ctx->params->params.nid_params.dsa.param_dsa_p,bn_ctx);
            }
          if (ret != OK) {
              fprintf(stderr, "mod exp problem\n");
              THROW_;
            }

          /* Compute bkeys */
          if(tmp_tree->parent->tgdh_nv->bkey != NULL){
              BN_clear_free(tmp_tree->parent->tgdh_nv->bkey);
              tmp_tree->parent->tgdh_nv->bkey=NULL;
            }
          tmp_tree->parent->tgdh_nv->bkey
              =tgdh_compute_bkey(tmp_tree->parent->tgdh_nv->key, ctx->params);

          if(tmp_tree->parent->parent == NULL) {
              break;
            }
          else{
              tmp_tree = tmp_tree->parent;
            }
          if(tmp_tree->tgdh_nv->index % 2){
              tmp1_tree = tmp_tree->parent->left;
            }
          else{
              tmp1_tree = tmp_tree->parent->right;
            }
        }
    }
    FINALLY
    {
      if(sponsor == 1){
          /* Creating token info
           * sets token{time, group name, message type and sender name}
           **/
          ret=tgdh_create_token_info(&info,ctx->group_name,TGDH_KEY_MERGE_UPDATE,
                                     time(0),ctx->member_name);
          /* Encoding */
          if(ret == 1){
              ret=tgdh_encode(ctx,output,info);
            }
          DEBUG_PRINT("encoding ctx info in output, output is %u\n", *(*output)->t_data);

          /* sign_message */
          if(ret == 1){
              ret=tgdh_sign_message (ctx, *output);
            }          
          DEBUG_PRINT("success! output is %u\n", *(*output)->t_data);

        }
      /* OK... Let's free the memory */
      if (ret!=OK) tgdh_destroy_ctx(&ctx, 1);
      if (info != (TGDH_TOKEN_INFO*)NULL) tgdh_destroy_token_info(&info);
      //if (bn_ctx != NULL) BN_CTX_free (bn_ctx);
    }
    ETRY;

    return ret;
  }

  /* tgdh_cascade is called by every member several times until every
   * member can compute the new group key when any network events occur.
   * o this function handles every membership event, e.g. join, leave,
   *   merge, and partition.
   * o this function handles any cascaded events.
   * o this funciton is tgdh-stabilizing.
   * o sponsors are decided uniquely for every membership event, and
   *   only the sponsors return an output
   *   - ctx: context of the caller
   *   - member_name: name of the caller
   *   - users_leaving: list of the leaving members, used only for
   *       subtractive events
   *   - input: Input token(previous output token of
   *       tgdh_cascade or join or merge request)
   *   - output: output token(will be used as next input token of
   *       tgdh_cascade)
   */
  int tgdh_cascade(TGDH_CONTEXT **ctx, GKA_NAME *group_name,
                   GKA_NAME *users_leaving[],
                   TOKEN_LIST *list, GKA_TOKEN **output){
    TGDH_TOKEN_INFO *info=NULL;
    int i=0;
    TGDH_SIGN *sign=NULL;
    int ret=CONTINUE;
    KEY_TREE *tmp_node=NULL, *tmp1_node=NULL;
    int num_sponsor=0;

    int new_information=0;
    int result=OK;
    int new_status=0;
    int new_key_comp=0;
    BN_CTX *bn_ctx=BN_CTX_new();
    KEY_TREE *sponsor_list[NUM_USERS+1]={NULL};
    int sponsor=0, sender=0;
    int leaveormerge=0;
    int message_type=-1;
    TOKEN_LIST *tmp_list=NULL;
    TREE_LIST *new_tree_list=NULL, *tmp_tree_list=NULL;
    TGDH_CONTEXT *new_ctx=NULL;
    int epoch=0;

    for(i=0; i<NUM_USERS+1; i++){
        sponsor_list[i]=NULL;
      }

    TRY
    {
      /* Doing some error checkings */
      if ((*ctx) == NULL) return CTX_ERROR;
      if (group_name == NULL) return INVALID_GROUP_NAME;
      if ((strlen(group_name) == 0) ||
          (strlen(group_name) > MAX_LGT_NAME)) return INVALID_LGT_NAME;

      /*    tgdh_print_simple("Initial", (*ctx)->root); */
      if(users_leaving != NULL){
          leaveormerge = 1;

          result = tgdh_remove_member(*ctx, users_leaving, sponsor_list);
          if(result == 4){
              result = (*ctx)->status;
              THROW_;
            }

          else{
              if(result < 0){
                  THROW_;
                }
            }

          i=0;
          if(sponsor_list[0] == NULL){
              fprintf(stderr, "No sponsor? Strange!!!\n");
              THROW_;
            }

          while(sponsor_list[i] != NULL){
              if(strcmp(sponsor_list[i]->tgdh_nv->member->member_name,
                        (*ctx)->member_name)== 0){
                  new_information = 1;
                  new_key_comp = 1;
                  if(sponsor_list[i]->tgdh_nv->key == NULL){
                      sponsor_list[i]->tgdh_nv->key=tgdh_rand((*ctx)->params);
                      sponsor_list[i]->tgdh_nv->bkey
                          =tgdh_compute_bkey(sponsor_list[i]->tgdh_nv->key,
                                             (*ctx)->params);
                    }
                  sponsor = 1;
                }
              i++;
            }
        }
      else{ /* This is not leave */
          if(list == NULL) {
              result = INVALID_INPUT_TOKEN;
              printf("\nList of tokens is empty, this is weird.\n\n");

              THROW_;
            }

          tmp_list = list;
          while (tmp_list != NULL){
              result=tgdh_remove_sign(tmp_list->token,&sign);
              if (result != OK) THROW_;

              /* Decoding the token & creating new_ctx */
              result=tgdh_decode(&new_ctx, tmp_list->token, &info);
              if(strcmp(info->sender_name, (*ctx)->member_name) == 0){
                  sender = 1;
                }
              epoch=MAX(new_ctx->epoch, (epoch<0)?(uint)-epoch:(uint)epoch);
              if (result!=OK){
                  THROW_;
                }
              new_status = new_ctx->status;
              if (strcmp(info->group_name,group_name)){
                  result=GROUP_NAME_MISMATCH;
                  THROW_;
                }
              /* Before merging the tree, we need to verify signature */
              result=tgdh_verify_sign ((*ctx), new_ctx, tmp_list->token,
                                       info->sender_name, sign);
              if(result!=OK){ puts("::tgdh_verify_sign failed somehow"); THROW_; }

              if(tmp_list->token != NULL){
                  result=tgdh_restore_sign(tmp_list->token,&sign);
                }
              DEBUG_PRINT("result of restore_sign is %d",result);

              tmp_node = tgdh_search_member(new_ctx->root, 4, (*ctx)->member_name);
              if(tmp_node != NULL){
                  if(tgdh_check_useful(new_ctx->root, (*ctx)->root)==1){
                      /* Copy new information(if any) to my context;           */
                      /* Tree should be same to process the following function */
                      tgdh_swap_bkey(new_ctx->root, (*ctx)->root);
                    }
                  tgdh_free_tree(&(new_ctx->root));
                  new_ctx->root = (*ctx)->root;
                }

              /* Add new_ctx, info to new_tree_list */
              new_tree_list = add_tree_list(new_tree_list, new_ctx->root);
              tmp_list = tmp_list->next;
              message_type = info->message_type;

              tgdh_destroy_ctx(&new_ctx, 0);
              tgdh_destroy_token_info(&info);
            }

          switch (message_type) { /* There is no leave or join event */

            /*****************/
            /*               */
            /* JOIN or MERGE */
            /*               */
            /*****************/
            case TGDH_KEY_MERGE_UPDATE:
              {

                (*ctx)->status = CONTINUE;
                leaveormerge = 1;

                (*ctx)->root = new_tree_list->tree;
                tmp_tree_list = new_tree_list->next;
                (*ctx)->epoch = MAX((epoch<0)?(uint)-epoch:(uint)epoch, (*ctx)->epoch);
                while(tmp_tree_list != NULL){
                    (*ctx)->root = tgdh_merge((*ctx)->root, tmp_tree_list->tree);
                    tmp_tree_list = tmp_tree_list->next;
                  }

                break;
              }
              /********************/
              /*                  */
              /* MEMBERSHIP EVENT */
              /*                  */
              /********************/
            case PROCESS_EVENT:
              {
                if((epoch<0)?(uint)-epoch:(uint)epoch != (*ctx)->epoch){
                    fprintf(stderr, "\nReceived: %d, Mine: %d\n", epoch,
                            (*ctx)->epoch);
                    result = UNSYNC_EPOCH;
                    THROW_;
                  }

                /* This includes second call of partition, update_ctx of */
                /* join and merge operation                              */
                if(((*ctx)->status == KEY_COMPUTED) && sender &&
                   (new_status == KEY_COMPUTED)){
                    (*ctx)->status = OK;
                    ret = OK;
                    THROW_;
                  }

                if(new_status == KEY_COMPUTED){
                    (*ctx)->status = OK;
                    ret = OK;
                    new_information = 1;
                  }

                tgdh_init_bfs((*ctx)->root);
                num_sponsor = find_sponsors((*ctx)->root, sponsor_list);
                tgdh_init_bfs((*ctx)->root);
                i=0;
                if(sponsor_list[0] == NULL){
                    fprintf(stderr, "No sponsor? Strange!!!\n");
                    THROW_;
                  }

                while(sponsor_list[i] != NULL){
                    if(strcmp(sponsor_list[i]->tgdh_nv->member->member_name,
                              (*ctx)->member_name)== 0){
                        new_information = 1;
                        new_key_comp = 1;
                        sponsor = 1;
                      }
                    i++;
                  }

                if(new_information == 0){
                    THROW_;
                  }
                break;
              }

              /* No more cases */
            default:
              {
                result=INVALID_MESSAGE_TYPE;
                THROW_;
              }
            }
        }

      tmp1_node = tgdh_search_member((*ctx)->root, 4, (*ctx)->member_name);
      if(tmp1_node == NULL){
          fprintf(stderr, "I cannot find me 222\n");
          result = STRUCTURE_ERROR;
          THROW_;
        }

      tgdh_init_bfs((*ctx)->root);
      num_sponsor = find_sponsors((*ctx)->root, sponsor_list);
      tgdh_init_bfs((*ctx)->root);
      i=0;
      if(sponsor_list[0] == NULL){
          fprintf(stderr, "No sponsor? Strange!!!\n");
          THROW_;
        }

      while (sponsor_list[i] != NULL)
        {
          if(strcmp(sponsor_list[i]->tgdh_nv->member->member_name,
                    (*ctx)->member_name)== 0){
              new_information = 1;
              new_key_comp = 1;
              sponsor = 1;
            }
          i++;
        }

      /* tgdh_print_simple("Bef comput", (*ctx)->root); */
      /* If not needed, only sponsor computes the key... */
      if(new_information || new_status == KEY_COMPUTED){
          tmp1_node = tgdh_search_member((*ctx)->root, 4, (*ctx)->member_name);
          if(tmp1_node == NULL){
              fprintf(stderr, "I cannot find me 3\n");
              result = STRUCTURE_ERROR;
              THROW_;
            }
          if(tmp1_node != (*ctx)->root){
              while(tmp1_node->parent != NULL)
                {
                  if(tmp1_node->parent->tgdh_nv->key == NULL){
                      break;
                    }
                  tmp1_node = tmp1_node->parent;
                }
            }

          if(tmp1_node != (*ctx)->root){
              if(tmp1_node->parent->tgdh_nv->key != NULL){
                  fprintf(stderr, "PArent not null 2\n");
                  result = STRUCTURE_ERROR;
                  THROW_;
                }
              if(tmp1_node->tgdh_nv->index % 2){
                  tmp_node = tmp1_node->parent->left;
                }
              else
                {
                  tmp_node = tmp1_node->parent->right;
                }
              while(tmp_node->tgdh_nv->bkey != NULL){
                  /* Compute intermediate keys until I can */
                  if(tmp1_node->parent->tgdh_nv->key != NULL){
                      fprintf(stderr, "Parent not null 2\n");
                      result=STRUCTURE_ERROR;
                      THROW_;
                    }
                  tmp1_node->parent->tgdh_nv->key = BN_new();
                  new_key_comp = 1;
//                  const BIGNUM *ctx_param_p, *ctx_param_q, *ctx_param_g;
//                  DSA_get0_pqg((*ctx)->params, &ctx_param_p, &ctx_param_q, &ctx_param_g);
                  if(tmp1_node->parent->left->tgdh_nv->key != NULL){
                      result = BN_mod(tmp1_node->parent->left->tgdh_nv->key,
                                      tmp1_node->parent->left->tgdh_nv->key,
                                      (const BIGNUM*)(*ctx)->params->params.nid_params.dsa.param_dsa_q, bn_ctx);
                      if(result != OK) THROW_;
                      result=BN_mod_exp(tmp1_node->parent->tgdh_nv->key,
                                        tmp1_node->parent->right->tgdh_nv->bkey,
                                        tmp1_node->parent->left->tgdh_nv->key,
                                        (const BIGNUM*)(*ctx)->params->params.nid_params.dsa.param_dsa_p,bn_ctx);
                    }
                  else
                    {
                      result = BN_mod(tmp1_node->parent->right->tgdh_nv->key,
                                      tmp1_node->parent->right->tgdh_nv->key,
                                      (const BIGNUM*)(*ctx)->params->params.nid_params.dsa.param_dsa_q, bn_ctx);
                      if(result != OK) THROW_;
                      result=BN_mod_exp(tmp1_node->parent->tgdh_nv->key,
                                        tmp1_node->parent->left->tgdh_nv->bkey,
                                        tmp1_node->parent->right->tgdh_nv->key,
                                        (const BIGNUM*)(*ctx)->params->params.nid_params.dsa.param_dsa_p,bn_ctx);
                    }
                  if(result != OK) THROW_;

                  /* Compute bkeys */
                  if(tmp1_node->parent->tgdh_nv->bkey != NULL){ // empty the parent bkey if not empty
                      BN_clear_free(tmp1_node->parent->tgdh_nv->bkey);
                      tmp1_node->parent->tgdh_nv->bkey=NULL;
                    }
                  tmp1_node->parent->tgdh_nv->bkey // actual computation
                      =tgdh_compute_bkey(tmp1_node->parent->tgdh_nv->key, (*ctx)->params);

                  if(tmp1_node->parent->parent == NULL) {
                      break;
                    }
                  else
                    {
                      tmp1_node = tmp1_node->parent;
                    }
                  if(tmp1_node->tgdh_nv->index % 2){
                      tmp_node = tmp1_node->parent->left;
                    }
                  else
                    {
                      tmp_node = tmp1_node->parent->right;
                    }
                }
            }
        }
      /* tgdh_print_simple("Aft comput", (*ctx)->root); */

      if((*ctx)->root->tgdh_nv->key != NULL){
          if((*ctx)->status == CONTINUE){
              ret = KEY_COMPUTED;
              (*ctx)->status = KEY_COMPUTED;
            }
          if((*ctx)->root->tgdh_nv->member != NULL){
              if(strcmp((*ctx)->root->tgdh_nv->member->member_name,
                        (*ctx)->member_name)==0){
                  ret = OK;
                  (*ctx)->status = OK;
                }
            }
        }

      if((new_information == 1) && (new_key_comp == 1) && ((*ctx)->status != OK)){
          /* Creating token info */
          result=tgdh_create_token_info(&info, (*ctx)->group_name,
                                        PROCESS_EVENT, time(0),
                                        (*ctx)->member_name);
          if (result!=OK) THROW_;

          result=tgdh_encode((*ctx),output,info);
          if (result!=OK) THROW_;

          /* Sign output token; */
          result=tgdh_sign_message ((*ctx), *output);
        }
    }
    FINALLY
    {
      if(new_tree_list != NULL){
          remove_tree_list(&new_tree_list);
        }

      if(ret == OK){
          if((*ctx)->root->tgdh_nv->key == NULL){
              fprintf(stderr, "Key is NULL, but return is OK\n\n");
            }
          BN_copy((*ctx)->group_secret,(*ctx)->root->tgdh_nv->key);
          result=tgdh_compute_secret_hash ((*ctx));
          (*ctx)->epoch++; /* Used inside tgdh_encode */
        }

      if (result <= 0){
          ret = result;
          if (ctx != NULL) tgdh_destroy_ctx(ctx,1);
        }
      if (info != NULL) tgdh_destroy_token_info(&info);
      if (bn_ctx != NULL) BN_CTX_free (bn_ctx);
    }
    ETRY;

    return ret;
  }

  /************************/
  /*TREE private functions*/
  /************************/

  int tgdh_create_ctx(TGDH_CONTEXT **ctx)
  {
    int ret=CTX_ERROR;

    if (*ctx != (TGDH_CONTEXT *)NULL) return CTX_ERROR;

    /* Creating ctx */
    TRY
    {
      (*ctx) = (TGDH_CONTEXT *) calloc(sizeof(TGDH_CONTEXT), 1);
      if ((*ctx) == NULL) THROW_;
      (*ctx)->member_name = (GKA_NAME *) NULL;
      (*ctx)->group_name = (GKA_NAME *) NULL;
      (*ctx)->root = (KEY_TREE *) calloc(sizeof(KEY_TREE), 1);
      if ((*ctx)->root == (KEY_TREE *) NULL) THROW_;
      (*ctx)->group_secret_hash = (gka_uchar*) calloc (SHA256_DIGEST_LENGTH, 1);
      if ((*ctx)->group_secret_hash == NULL) THROW_;
      (*ctx)->params = (GKA_KEY *) calloc(sizeof(GKA_KEY),1);
      (*ctx)->pkey = (GKA_KEY *) calloc(sizeof(GKA_KEY),1);

      (*ctx)->root->parent=(*ctx)->root->left=(*ctx)->root->right = NULL;
      (*ctx)->root->prev=(*ctx)->root->next=(*ctx)->root->bfs = NULL;
      (*ctx)->epoch = 0;

      ret=OK;
    }
    FINALLY
    {
      if (ret!=OK) { tgdh_destroy_ctx (ctx,1); }
    }
    ETRY;

    return ret;
  }

  /* Frees the space occupied by the current context.
   * Including the group_members_list.
   *   if flag == 1, delete all context
   *   if flag == 0, delete all except the tree(used for merge)
   */
  void tgdh_destroy_ctx (TGDH_CONTEXT **ctx, int flag)
  {

    if ((*ctx) == NULL) return;
    if (((*ctx)->member_name) != NULL) {
        free((*ctx)->member_name);
        (*ctx)->member_name=NULL;
      }
    if (((*ctx)->group_name) != NULL) {
        free((*ctx)->group_name);
        (*ctx)->group_name=NULL;
      }
    if (((*ctx)->group_secret) != NULL) {
        BN_clear_free((*ctx)->group_secret);
        (*ctx)->group_secret=NULL;
      }
    if (((*ctx)->tmp_key) != NULL) {
        BN_clear_free((*ctx)->tmp_key);
        (*ctx)->tmp_key=NULL;
      }
    if (((*ctx)->tmp_bkey) != NULL) {
        BN_clear_free((*ctx)->tmp_bkey);
        (*ctx)->tmp_bkey=NULL;
      }
    if (((*ctx)->group_secret_hash) != NULL) {
        free((*ctx)->group_secret_hash);
        (*ctx)->group_secret_hash=NULL;
      }

    if(flag == 1){
        tgdh_free_tree(&((*ctx)->root));
        (*ctx)->root=NULL;
      }

    if (((*ctx)->params) != NULL) {
        switch ((*ctx)->params->params.nid) {
          case NID_rsa:
            BN_free((*ctx)->params->params.nid_params.rsa.param_rsa_d);
            BN_free((*ctx)->params->params.nid_params.rsa.param_rsa_e);
            BN_free((*ctx)->params->params.nid_params.rsa.param_rsa_n);
            break;
          case NID_dsa:
            BN_free((*ctx)->params->params.nid_params.dsa.param_dsa_g);
            BN_free((*ctx)->params->params.nid_params.dsa.param_dsa_p);
            BN_free((*ctx)->params->params.nid_params.dsa.param_dsa_q);
            break;
          }
        (*ctx)->params=NULL;
      }
    if (((*ctx)->pkey->private_key) != NULL) {
        EVP_PKEY_free((*ctx)->pkey->private_key);
        (*ctx)->pkey->private_key=NULL;
      }
    if (((*ctx)->epoch) != (int)NULL) {
        (*ctx)->epoch = (int)NULL;
      }
    free((*ctx));
    (*ctx)=NULL;

    return;
  }

  /* TODO: write an equivalent to encode/decode functions, which I removed */
  /* tgdh_encode using information from the current context and from
   * token info generates the output token.
   * include_last_partial: If TRUE includes all last_partial_keys,
   * otherwise it includes the partial key of the (controller) first
   * user in ckd. Hence it should be TRUE if called within cliques and
   * FALSE if called from ckd_gnrt_gml.
   *
   * Note: output is created here.
   * Preconditions: *output should be empty (otherwise it will be
   * freed).
   */
  int tgdh_encode(TGDH_CONTEXT *ctx, GKA_TOKEN **output,
                  TGDH_TOKEN_INFO *info)
  {
    uint pos=0;
    gka_uchar *data;

    /* Freeing the output token if necessary */
    if((*output) != NULL) tgdh_destroy_token(output);

    /* Do some error checkings HERE !! */
    if (ctx == (TGDH_CONTEXT *) NULL) return CTX_ERROR;
    /* The token has to match the current group name */
    if (strcmp(info->group_name,ctx->group_name)) return GROUP_NAME_MISMATCH;
    /* Done with error checkings */

    data=(gka_uchar *) calloc (sizeof(gka_uchar)*MSG_SIZE,1);
    if (data==(gka_uchar *) NULL) return MALLOC_ERROR;

    string_encode(data,&pos,info->group_name);
    int_encode(data,&pos,info->message_type);
    int_encode(data,&pos,(uint)info->time_stamp);
    /* Note: info->sender_name is not used here. The name is retreived
     * from ctx.
     */
    DEBUG_PRINT("\t::tgdh_encode ctx->member_name is %s\n",ctx->member_name);
    DEBUG_PRINT("\t::tgdh_encode info->group_name is %s\n",info->group_name);
    string_encode(data,&pos,ctx->member_name);
    int_encode(data,&pos,ctx->epoch);
    int_encode(data,&pos,(uint)ctx->status);

    tgdh_map_encode(data, &pos, ctx->root);

    DEBUG_PRINT("\t::tgdh_encode data is %zu bytes\n",sizeof(gka_uchar)*pos);
    /*for(uint i=0;i<(sizeof(gka_uchar)*pos);i++){
        printf("%02x ",*data+i);
      }
    printf("\n");*/

    *output=(GKA_TOKEN *) calloc(sizeof(GKA_TOKEN),1);
    if (*output == (GKA_TOKEN *) NULL) return MALLOC_ERROR;
    (*output)->length=pos;
    (*output)->t_data=data;

    return OK;
  }

  /* Converts tree structure to unsigned character string */
  void tgdh_map_encode(gka_uchar *stream, uint *pos, KEY_TREE *root)
  {
    KEY_TREE *head, *tail;
    int map = 0;        /* If map is 3, index, bkey, member_name
                       * If map is 2, index, member_name
                       * If map is 1, index, bkey
                       * If map is 0, only index
                       */

    tgdh_init_bfs(root);

    int_encode(stream, pos, (uint)root->tgdh_nv->height);
    int_encode(stream, pos, root->tgdh_nv->num_node);
    head = tail = root;

    while(head != NULL){
        if(head->tgdh_nv->member == NULL){
            if(head->tgdh_nv->bkey == NULL){
                map = 0;
              }
            else map = 1;
          }
        else{
            if(head->tgdh_nv->bkey == NULL){
                map = 2;
              }
            else map = 3;
          }

        /* Real encoding */
        int_encode(stream, pos, map);
        int_encode(stream, pos, head->tgdh_nv->index);
        int_encode(stream, pos, (uint)head->tgdh_nv->potential);
        int_encode(stream, pos, head->tgdh_nv->joinQ);
        if(head->tgdh_nv->bkey != NULL)
          bn_encode(stream, pos, head->tgdh_nv->bkey);
        if(head->tgdh_nv->member != NULL)
          string_encode(stream, pos, head->tgdh_nv->member->member_name);

        /* Queue handling */
        if(head->left){
            tail->bfs = head->left;
            tail = tail->bfs;
          }
        if(head->right){
            tail->bfs = head->right;
            tail = tail->bfs;
          }
        head = head->bfs;
      }

    tgdh_init_bfs(root);
  }

  /* tgdh_decode using information from the input token, it creates
   * ctx. info is also created here. It contains data recovered from
   * input such as message_type, sender, etc. (See structure for more
   * details) in readable format.
   * Preconditions: *ctx has to be NULL.
   * Postconditions: ctx is created. The only valid data in it is
   * group_members_list (first & last), and epoch. All the other
   * variables are NULL. (tgdh_create_ctx behavior)
   */
  int tgdh_decode(TGDH_CONTEXT **ctx, GKA_TOKEN *input,
                  TGDH_TOKEN_INFO **info)
  {
    GKA_NAME *group = (GKA_NAME*)"";
    GKA_NAME *sender = (GKA_NAME*)"";
    uint pos=0;
    int ret=CTX_ERROR;

    if (input == NULL
        || input->t_data == NULL
        || input->length <= 0) {
        return INVALID_INPUT_TOKEN;
      }

    /* Creating token info */
    TRY
    {
      ret = tgdh_create_token_info(info, group, TGDH_INVALID, 0L, sender);
      if (ret != OK)
        THROW_;

      if (ret != tgdh_create_ctx(ctx))
        THROW_;

      ret=INVALID_INPUT_TOKEN;
      if (!string_decode(input,&pos,(*info)->group_name))
        THROW_;
      if (!int_decode(input,&pos,(uint*)&(*info)->message_type))
        THROW_;
      if (!int_decode(input,&pos,(uint *)&(*info)->time_stamp))
        THROW_;
      if (!string_decode(input,&pos,(*info)->sender_name))
        THROW_;
      if (!int_decode(input,&pos,&(*ctx)->epoch))
        THROW_;
      if (!int_decode(input,&pos,(gka_uint*)&(*ctx)->status))
        THROW_;
      if ((ret=tgdh_map_decode(input,&pos,ctx)) != OK)
        THROW_;

      /* Checking after decoding */
      if (
          (((*info)->sender_name) == NULL)
          || (((*info)->group_name) == NULL)
          //|| ((*ctx)->epoch < 0)
         )
        ret=INVALID_INPUT_TOKEN;
      else
        ret=OK;
    }
    FINALLY
    {
      if (ret != OK) {
          if (info != NULL) tgdh_destroy_token_info(info);
          if (ctx != NULL) tgdh_destroy_ctx(ctx,1);
        }
    }
    ETRY;

    return ret;
  }

  /* tgdh_map_decode decode input token to generate tree for the new
   *   tree
   * *tree should be pointer to the root node
   */
  int tgdh_map_decode(const GKA_TOKEN *input, uint *pos,
                      TGDH_CONTEXT **ctx)
  {
    int i;
    uint map=0;
    uint tmp_index;
    KEY_TREE *tmp_tree=NULL, *tmp1_tree=NULL;
    int ret=OK;

    TRY
    {
      (*ctx)->root->tgdh_nv = (TGDH_NV *)calloc(sizeof(TGDH_NV),1);
      if ((*ctx)->root->tgdh_nv == NULL)
        {ret=MALLOC_ERROR; THROW_;}
      if(!int_decode(input, pos, (uint *)&((*ctx)->root->tgdh_nv->height)))
        return 0;
      if(!int_decode(input, pos, (uint *)&((*ctx)->root->tgdh_nv->num_node)))
        return 0;

      (*ctx)->root->parent = NULL;
      (*ctx)->root->tgdh_nv->member = NULL;

      if(!int_decode(input, pos, &map)) return 0;
      if(!int_decode(input, pos, &tmp_index)) return 0;

      (*ctx)->root->tgdh_nv->index = tmp_index;
      if(!int_decode(input, pos, (uint *)&((*ctx)->root->tgdh_nv->potential)))
        return 0;
      if(!int_decode(input, pos, &((*ctx)->root->tgdh_nv->joinQ)))
        return 0;

      (*ctx)->root->tgdh_nv->key = (*ctx)->root->tgdh_nv->bkey = NULL;
      if(map & 0x1){
          (*ctx)->root->tgdh_nv->bkey = BN_new();
          if(!bn_decode(input, pos, (*ctx)->root->tgdh_nv->bkey)) return 0;
        }
      if((map >> 1) & 0x1){
          (*ctx)->root->tgdh_nv->member
              = (TGDH_GM *)calloc(sizeof(TGDH_GM),1);
          if((*ctx)->root->tgdh_nv->member == NULL)
            {ret=MALLOC_ERROR; THROW_;}

          (*ctx)->root->tgdh_nv->member->cert = NULL;
          (*ctx)->root->tgdh_nv->member->member_name =
              (GKA_NAME *)calloc(sizeof(GKA_NAME)*MAX_LGT_NAME,1);
          if((*ctx)->root->tgdh_nv->member->member_name == NULL)
            {ret=MALLOC_ERROR; THROW_;}
          if(!string_decode(input, pos,
                            (*ctx)->root->tgdh_nv->member->member_name))
            return 0;
        }

      for(i=0; i<(*ctx)->root->tgdh_nv->num_node-1; i++){
          if(!int_decode(input, pos, &map)) return 0;
          if(!int_decode(input, pos, &tmp_index)) return 0;
          tmp_tree=tgdh_search_index((*ctx)->root, tmp_index);
          if(tmp_tree == NULL) return 0;
          tmp_tree->tgdh_nv->member = NULL;
          tmp1_tree = (KEY_TREE *)calloc(sizeof(KEY_TREE),1);

          tmp1_tree->parent = tmp_tree;
          if(tmp_index % 2)
            tmp_tree->right = tmp1_tree;
          else tmp_tree->left = tmp1_tree;
          tmp1_tree->tgdh_nv = (TGDH_NV *)calloc(sizeof(TGDH_NV),1);
          tmp1_tree->tgdh_nv->member = NULL;
          tmp1_tree->tgdh_nv->key = tmp1_tree->tgdh_nv->bkey = NULL;
          tmp1_tree->tgdh_nv->index = tmp_index;
          tmp1_tree->tgdh_nv->height = tmp1_tree->tgdh_nv->num_node=-1;
          tmp1_tree->left=tmp1_tree->right=NULL;
          tmp1_tree->prev=tmp1_tree->next=tmp1_tree->bfs=NULL;
          if(!int_decode(input, pos, (uint *)&(tmp1_tree->tgdh_nv->potential)))
            return 0;
          if(!int_decode(input, pos, &(tmp1_tree->tgdh_nv->joinQ)))
            return 0;
          if(map & 0x1){
              tmp1_tree->tgdh_nv->bkey = BN_new();
              if(!bn_decode(input, pos, tmp1_tree->tgdh_nv->bkey)) return 0;
            }
          if((map >> 1) & 0x1){
              tmp1_tree->tgdh_nv->member=(TGDH_GM *)calloc(sizeof(TGDH_GM),1);
              tmp1_tree->tgdh_nv->member->member_name =
                  (GKA_NAME *)calloc(sizeof(GKA_NAME)*MAX_LGT_NAME,1);
              tmp1_tree->tgdh_nv->member->cert=NULL;

              if(!string_decode(input, pos, tmp1_tree->tgdh_nv->member->member_name))
                return 0;
              tmp_tree = tgdh_search_member(tmp1_tree, 0, NULL);
              tmp1_tree->prev = tmp_tree;
              if(tmp_tree != NULL) tmp_tree->next = tmp1_tree;
              tmp_tree = tgdh_search_member(tmp1_tree, 1, NULL);
              tmp1_tree->next = tmp_tree;
              if(tmp_tree != NULL) tmp_tree->prev = tmp1_tree;
              tmp1_tree->bfs = NULL;
            }
        }
    }
    FINALLY
    {
      if(ret != OK){
          if((*ctx)->root->tgdh_nv->member != NULL){
              if((*ctx)->root->tgdh_nv->member->member_name != NULL)
                free((*ctx)->root->tgdh_nv->member->member_name);
              free((*ctx)->root->tgdh_nv->member);
            }
          if((*ctx)->root->tgdh_nv != NULL) free((*ctx)->root->tgdh_nv);
        }
    }
    ETRY;

    return ret;
  }


  /* tgdh_create_token_info: It creates the info token. */
  int tgdh_create_token_info (TGDH_TOKEN_INFO **info, const GKA_NAME *group,
                              enum TGDH_MSG_TYPE msg_type, time_t time,
                              GKA_NAME *sender/*, uint epoch*/)
  {
    int ret=MALLOC_ERROR;

    /* Creating token information */
    TRY
    {
      (*info)=(TGDH_TOKEN_INFO *) calloc (sizeof(TGDH_TOKEN_INFO),1);
      if ((*info) == NULL) THROW_;
      if (group != NULL) {
          (*info)->group_name
              =(GKA_NAME *) calloc(sizeof(GKA_NAME)*MAX_LGT_NAME,1);
          if (((*info)->group_name) == NULL) THROW_;
          strncpy ((*info)->group_name,group,MAX_LGT_NAME);
        } else (*info)->group_name=NULL;
      (*info)->message_type=msg_type;
      (*info)->time_stamp=time;
      if (sender != NULL) {
          (*info)->sender_name=(GKA_NAME *)
              calloc(sizeof(GKA_NAME)*MAX_LGT_NAME,1);
          if (((*info)->sender_name) == NULL) THROW_;
          strncpy ((*info)->sender_name,sender,MAX_LGT_NAME);
        }
      else (*info)->sender_name=NULL;
      /*  (*info)->epoch=epoch; */

      ret=OK;
    }
    CATCH(GENERIC_THROW)
    {
      puts("::tgdh_create_token_info threw");
    }
    FINALLY
    {
      if (ret != OK) tgdh_destroy_token_info(info);
    }
    ETRY;

    return ret;
  }

  /* tgdh_destroy_token: It frees the memory of the token. */
  void tgdh_destroy_token (GKA_TOKEN **token) {
    if (*token !=(GKA_TOKEN *) NULL) {
        if ((*token)->t_data != NULL) {
            free ((*token)->t_data);
            (*token)->t_data=NULL;
          }
        free(*token);
        *token=NULL;
      }
  }

  /* tgdh_destroy_token_info: It frees the memory of the token. */
  void tgdh_destroy_token_info (TGDH_TOKEN_INFO **info)
  {
    if (info == NULL) return;
    if ((*info) == NULL) return;
    if ((*info)->group_name != NULL) {
        free ((*info)->group_name);
        (*info)->group_name =NULL;
      }
    if ((*info)->sender_name != NULL) {
        free ((*info)->sender_name);
        (*info)->sender_name=NULL;
      }
    free ((*info));
    *info = NULL;
  }

  /* tgdh_merge_tree returns root of a new tree which is the result of
 * merge of two trees
 */
  KEY_TREE *tgdh_merge_tree(KEY_TREE *joiner, KEY_TREE *joinee)
  {
    KEY_TREE *tmp_tree=NULL, *tmp_root=NULL;
    KEY_TREE *last=NULL, *last1=NULL, *first=NULL;
    int height=-1;

    if(joiner->parent != NULL) return NULL;

    tmp_tree = (KEY_TREE *)calloc(sizeof(KEY_TREE),1);
    if(tmp_tree == NULL) return NULL;

    /* setting up pointers for the new node */
    tmp_tree->parent = joinee->parent;
    tmp_tree->left = joinee;
    tmp_tree->right = joiner;
    tmp_tree->prev=tmp_tree->next=tmp_tree->bfs=NULL;

    /* setting up pointer for the parent of the joinee */
    if(joinee->parent != NULL){
        if(joinee->tgdh_nv->index % 2)
          joinee->parent->right = tmp_tree;
        else joinee->parent->left = tmp_tree;
      }

    /* setting up pointers for the joinee and joiner */
    joiner->parent = tmp_tree;
    joinee->parent = tmp_tree;

    /* setting up the pointers for the leaf nodes */
    last = tgdh_search_member(joinee, 3, NULL);
    first = tgdh_search_member(joiner, 2, NULL);
    last1 = tgdh_search_member(joiner, 3, NULL);
    if(last->next != NULL){
        last1->next = last->next;
        last->next->prev = last1;
      }
    last->next = first;
    first->prev = last;

    /* Now, real values for the tmp_tree */
    tmp_tree->tgdh_nv=(TGDH_NV *)calloc(sizeof(TGDH_NV),1);
    tmp_tree->tgdh_nv->index = tmp_tree->left->tgdh_nv->index;
    tmp_tree->tgdh_nv->key = tmp_tree->tgdh_nv->bkey = NULL;

    /* First, I'm just updating index... And then I'll update
   * potential using brute-force */
    tgdh_update_index(tmp_tree->right, 1, tmp_tree->tgdh_nv->index);
    tgdh_update_index(tmp_tree->left, 0, tmp_tree->tgdh_nv->index);

    tmp_tree->tgdh_nv->member = NULL;

    /* Potential will be updated here */
    tmp_root = tmp_tree;
    while(tmp_root->parent != NULL)
      tmp_root = tmp_root->parent;

    first = tgdh_search_member(tmp_root, 2, NULL);
    while(first != NULL){
        height = MAX(height, log2(first->tgdh_nv->index));
        first = first->next;
      }

    tmp_root->tgdh_nv->height = height; /* update height */
    first = tgdh_search_member(tmp_root, 2, NULL);
    while(first != NULL){ /* Update potential for leaf users */
        first->tgdh_nv->potential
            = height - log2(first->tgdh_nv->index) - 1;
        if(first->tgdh_nv->potential == -1)
          first->tgdh_nv->joinQ = FALSE;
        else first->tgdh_nv->joinQ = TRUE;
        first = first ->next;
      }
    tgdh_update_potential(tmp_root);

    /* Now update some values upto the root */
    tgdh_update_key_path(&tmp_root);

    last=last1=first=NULL;

    return tmp_root;
  }

  /* remove_sponsor: remove the sponsor from the sponsor list */
  int remove_sponsor(GKA_NAME *sponsor_list[], GKA_NAME *sponsor)
  {
    int i=0, j=0;

    if(sponsor_list[0] == NULL) {
        return -1;
      }

    for(i=0; i<NUM_USERS+1; i++){
        if(sponsor_list[i] != NULL){
            if(strcmp(sponsor_list[i], sponsor)==0){
                break;
              }
          }
      }
    for(j=i; j<NUM_USERS-1; j++){
        if(sponsor_list[j] != NULL){
            sponsor_list[j] = sponsor_list[j+1];
          }
      }

    return 1;
  }

  /* remove_member removes leaving members from the current tree.
   * o Reason for this function: It was leave part of tgdh_cascade
   *    function, but I decided to make a function since we need to add
   *    this functionality to tgdh_merge_req too...
   * o What is it doing?
   *   - This function will only remove the leaving members...
   *   - No key update happens...
   */
  int tgdh_remove_member(TGDH_CONTEXT *ctx, GKA_NAME *users_leaving[],
                         KEY_TREE *sponsor_list[])
  {
    KEY_TREE *the_sponsor=NULL;
    int i=0;
    KEY_TREE *leave_node=NULL, *first=NULL;
    KEY_TREE *tmp_node=NULL, *tmp1_node=NULL;
    int min_index=100000;
    int height=0;
    int num_sponsor=0;

    while(users_leaving[i] != NULL)
      {
        /* If I have cache and if I receive another membership, remove
         * cache */
        ctx->status = CONTINUE;

        /* If my name is in the users_leaving list, then I just exit
         * partition with OK... I will call again to partition out other
         * members
         */
        if (strcmp(users_leaving[i], ctx->member_name)==0){
            return 4;
          }

        /* Delete every bkey and key which is related with the leaving
         * members;
         */
        tmp1_node=leave_node=tgdh_search_member(ctx->root, 4,
                                                users_leaving[i]);
        if (tmp1_node == NULL) {
            return MEMBER_NOT_IN_GROUP;
          }
        while (tmp1_node != NULL){
            if(tmp1_node->tgdh_nv->bkey != NULL){
                BN_clear_free(tmp1_node->tgdh_nv->bkey);
                tmp1_node->tgdh_nv->bkey = NULL;
              }
            if(tmp1_node->tgdh_nv->key != NULL){
                BN_clear_free(tmp1_node->tgdh_nv->key);
                tmp1_node->tgdh_nv->key = NULL;
              }
            tmp1_node = tmp1_node->parent;
          }

        /* Delete the leaving members from the current tree;
         * Pointer handling
         */
        if(leave_node->parent->parent == NULL){ /* If my index is 2 or 3 */
            if(leave_node->tgdh_nv->index % 2 == 0){ /* If my index is 2 */
                if(leave_node->parent != ctx->root){
                    fprintf(stderr, "Parent of leave node is not root 1\n");
                    return STRUCTURE_ERROR;
                  }
                tgdh_copy_node(leave_node->parent, leave_node->parent->right);
                tmp_node=ctx->root = leave_node->parent->right;
                leave_node->parent->right->parent = NULL;
              }
            else
              { /* If my index is 3 */
                if(leave_node->parent != ctx->root){
                    fprintf(stderr, "Parent of leave node is not root 1\n");
                    return STRUCTURE_ERROR;
                  }
                tgdh_copy_node(leave_node->parent, leave_node->parent->left);
                tmp_node=ctx->root = leave_node->parent->left;
                leave_node->parent->left->parent = NULL;
              }
          }
        else{
            if(leave_node->parent->tgdh_nv->index % 2 == 0){
                if(leave_node->tgdh_nv->index % 2 == 0){
                    tgdh_copy_node(leave_node->parent, leave_node->parent->right);
                    leave_node->parent->parent->left = leave_node->parent->right;
                    leave_node->parent->right->parent = leave_node->parent->parent;
                    tmp_node = leave_node->parent->right;
                  }
                else
                  {
                    tgdh_copy_node(leave_node->parent, leave_node->parent->left);
                    leave_node->parent->parent->left = leave_node->parent->left;
                    leave_node->parent->left->parent = leave_node->parent->parent;
                    tmp_node = leave_node->parent->left;
                  }
              }
            else{
                if(leave_node->tgdh_nv->index % 2 == 0){
                    tgdh_copy_node(leave_node->parent, leave_node->parent->right);
                    leave_node->parent->parent->right = leave_node->parent->right;
                    leave_node->parent->right->parent = leave_node->parent->parent;
                    tmp_node = leave_node->parent->right;
                  }
                else
                  {
                    tgdh_copy_node(leave_node->parent, leave_node->parent->left);
                    leave_node->parent->parent->right = leave_node->parent->left;
                    leave_node->parent->left->parent = leave_node->parent->parent;
                    tmp_node = leave_node->parent->left;
                  }
              }
          }
        if(tmp_node->left){
            tgdh_update_index(tmp_node->right, 1,
                              tmp_node->tgdh_nv->index);
            tgdh_update_index(tmp_node->left, 0,
                              tmp_node->tgdh_nv->index);
          }

        if(leave_node->prev != NULL)
          leave_node->prev->next = leave_node->next;
        if(leave_node->next != NULL)
          leave_node->next->prev = leave_node->prev;
        tgdh_free_node(&leave_node->parent);
        tgdh_free_node(&leave_node);

        ctx->root->tgdh_nv->num_node -= 2;
        first = tgdh_search_member(ctx->root, 2, NULL);
        if(first == NULL) {
            return STRUCTURE_ERROR;
          }
        height=0;
        while(first != NULL){
            height = MAX(log2(first->tgdh_nv->index), height);
            first = first->next;
          }

        ctx->root->tgdh_nv->height = height;
        first = tgdh_search_member(ctx->root, 2, NULL);
        if (first == NULL) {
            return STRUCTURE_ERROR;
          }
        while (first != NULL){
            first->tgdh_nv->potential =
                height - log2(first->tgdh_nv->index) -1;
            if(first->tgdh_nv->potential > -1) first->tgdh_nv->joinQ = TRUE;
            else first->tgdh_nv->joinQ = FALSE;
            first = first->next;
          }

        tgdh_update_potential(ctx->root);

        i++;
      }

    tgdh_init_bfs(ctx->root);
    num_sponsor = find_sponsors(ctx->root, sponsor_list);
    tgdh_init_bfs(ctx->root);

    /* tgdh_print_simple("Bef spo ch", ctx->root); */
    if(sponsor_list[0] != NULL){
        for(i=0; i<num_sponsor; i++){
            tmp1_node = tgdh_search_member(ctx->root, 4,
                                           sponsor_list[i]->tgdh_nv->member->member_name);
            if (tmp1_node->tgdh_nv->index < (min_index<0)?(uint)-min_index:(uint)min_index){
                the_sponsor = tmp1_node;
                min_index = tmp1_node->tgdh_nv->index;
              }
          }

        tmp1_node = the_sponsor;
        if (tmp1_node->tgdh_nv->bkey != NULL){
            BN_clear_free(tmp1_node->tgdh_nv->bkey);
            tmp1_node->tgdh_nv->bkey = NULL;
          }
        if (tmp1_node->tgdh_nv->key != NULL){
            BN_clear_free(tmp1_node->tgdh_nv->key);
            tmp1_node->tgdh_nv->key = NULL;
          }
        tmp1_node = tmp1_node->parent;
        while (tmp1_node != NULL){
            if (tmp1_node->tgdh_nv->bkey != NULL){
                BN_clear_free(tmp1_node->tgdh_nv->bkey);
                tmp1_node->tgdh_nv->bkey = NULL;
              }
            if (tmp1_node->tgdh_nv->key != NULL){
                BN_clear_free(tmp1_node->tgdh_nv->key);
                tmp1_node->tgdh_nv->key = NULL;
              }
            tmp1_node = tmp1_node->parent;
          }
      }

    return 1;
  }

  /* Make a tree list for merge */
  TREE_LIST *add_tree_list(TREE_LIST *list, KEY_TREE *tree)
  {
    TREE_LIST *tmp_list=NULL, *tmp1_list=NULL, *tmp2_list=NULL;
    TREE_LIST *head=NULL;
    KEY_TREE *tmp_tree=NULL, *tmp1_tree=NULL;

    head = list;
    tmp_tree = tgdh_search_member(tree, 3, NULL);

    if(tree == NULL) return NULL;

    if(list == NULL){
        list = (TREE_LIST *) calloc(sizeof(TREE_LIST), 1);
        if (list == NULL)
          return NULL;
        list->tree = tree;
        list->end = list;
        list->next = NULL;
        return list;
      }
    else
      {
        if (list->end != list){
            if (list->next == NULL)
              return NULL;
          }
        else
          {
            if(list->end->next != NULL)
              return NULL;
          }
        tmp_list = (TREE_LIST *) calloc(sizeof(TREE_LIST), 1);
        tmp_list->tree = tree;

        tmp1_list = head;
        while(tmp1_list != NULL){
            tmp1_tree = tgdh_search_member(tmp1_list->tree, 3, NULL);
            if(tmp1_list->tree->tgdh_nv->height <
               tmp_list->tree->tgdh_nv->height){
                break;
              }
            else if((tmp1_list->tree->tgdh_nv->height ==
                     tmp_list->tree->tgdh_nv->height) &&
                    (strcmp(tmp1_tree->tgdh_nv->member->member_name,
                            tmp_tree->tgdh_nv->member->member_name) < 0))
              {
                break;
              }
            else{
                tmp2_list = tmp1_list;
                tmp1_list = tmp1_list->next;
              }
          }

        if(tmp2_list == NULL){
            tmp_list->next = tmp1_list;
            tmp_list->end = tmp1_list->end;
            tmp1_list->end = NULL;
            return tmp_list;
          }

        if(tmp1_list == NULL){
            tmp2_list->next = tmp_list;
            head->end = tmp_list;
            if(tmp2_list != head){
                tmp2_list->end = NULL;
              }
            return head;
          }

        tmp2_list->next = tmp_list;
        tmp_list->next = tmp1_list;

      }

    return head;
  }

  int find_sponsors(KEY_TREE *root, KEY_TREE *sponsor_list[])
  {
    KEY_TREE *head=NULL, *tail=NULL, *holes[NUM_USERS]={NULL};
    int num_holes=0, i=0;

    for(i=0; i<NUM_USERS; i++){
        holes[i] = NULL;
      }

    head = tgdh_search_member(root, 5, NULL);
    head = tgdh_search_member(head, 2, NULL);
    tgdh_init_bfs(head);

    head = tail = root;

    while(head != NULL){
        if(head->left == NULL){
            holes[num_holes] = head;
            num_holes++;
          }
        else
          {
            if((head->left->tgdh_nv->bkey != NULL) && (head->right->tgdh_nv->bkey != NULL)){
                holes[num_holes] = head;
                num_holes++;
              }
            else
              {
                if(head->right->tgdh_nv->bkey == NULL){
                    tail->bfs = head->right;
                    tail = tail->bfs;
                  }
                if(head->left->tgdh_nv->bkey == NULL){
                    tail->bfs = head->left;
                    tail = tail->bfs;
                  }
              }
          }
        head = head->bfs;
      }

    head = tgdh_search_member(root, 5, NULL);
    head = tgdh_search_member(head, 2, NULL);
    tgdh_init_bfs(head);

    tail = head = NULL;

    for(i=0; i<num_holes; i++){
        sponsor_list[i] = tgdh_search_member(holes[i], 6, NULL);
      }

    return num_holes;
  }

  /* tgdh_merge merges two tree using tgdh_merge_tree */
  KEY_TREE *tgdh_merge(KEY_TREE *big_tree, KEY_TREE *small_tree)
  {
    KEY_TREE *tmp1_node=NULL, *tmp_node=NULL;
    KEY_TREE *joiner=NULL, *joinee=NULL;
    int tmp_height=0, tmp_num=0;

    /* Now, we can merge two trees to generate a new tree   */
    if(big_tree->tgdh_nv->height == small_tree->tgdh_nv->height){
        tmp1_node = tgdh_search_member(big_tree, 2, NULL);
        tmp_node = tgdh_search_member(small_tree, 2, NULL);
        if(strcmp(tmp1_node->tgdh_nv->member->member_name,
                  tmp_node->tgdh_nv->member->member_name)>0){
            joiner = small_tree;
            joinee = big_tree;
          }
        else if(strcmp(tmp1_node->tgdh_nv->member->member_name,
                       tmp_node->tgdh_nv->member->member_name)<0){
            joiner = big_tree;
            joinee = small_tree;
          }
        else
          {
            fprintf(stderr,"strange... two of them are same???\n");
            return NULL;
          }
      }
    else if(big_tree->tgdh_nv->height > small_tree->tgdh_nv->height){
        joiner = small_tree;
        joinee = big_tree;
      }
    else
      {
        joiner = big_tree;
        joinee = small_tree;
      }

    if(joiner->tgdh_nv->height <= joinee->tgdh_nv->potential){
        tmp_height = joinee->tgdh_nv->height;
      }
    else
      {
        tmp_height = joinee->tgdh_nv->height+1;
      }
    tmp_num=joiner->tgdh_nv->num_node+joinee->tgdh_nv->num_node+1;
    tmp1_node = tmp_node = tgdh_search_node(joiner, joinee, 0);
    tmp_node = tgdh_merge_tree(joiner, tmp_node);
    if(tmp_node == NULL) {
        return NULL;
      }
    if(tmp_node->parent != NULL){
        return NULL;
      }

    tmp1_node = joiner->parent;
    while(tmp1_node != NULL){
        if(tmp1_node->tgdh_nv->key != NULL){
            BN_clear_free(tmp1_node->tgdh_nv->key);
            tmp1_node->tgdh_nv->key = NULL;
          }
        if(tmp1_node->tgdh_nv->bkey != NULL){
            BN_clear_free(tmp1_node->tgdh_nv->bkey);
            tmp1_node->tgdh_nv->bkey = NULL;
          }
        tmp1_node = tmp1_node->parent;
      }

    big_tree = tmp_node;

    big_tree->tgdh_nv->height = tmp_height;
    big_tree->tgdh_nv->num_node = tmp_num;
    if(joinee != tmp_node){
        joinee->tgdh_nv->height=joinee->tgdh_nv->num_node=-1;
      }
    if(joiner != tmp_node){
        joiner->tgdh_nv->height=joiner->tgdh_nv->num_node=-1;
      }

    return big_tree;
  }

  /* Remove all tree list */
  void remove_tree_list(TREE_LIST **list)
  {
    TREE_LIST *tmp_list=NULL;

    tmp_list = (*list)->next;
    while((*list) != NULL){
        free(*list);
        (*list) = tmp_list;
        if((*list) == NULL){
            break;
          }
        else
          {
            tmp_list = (*list)->next;
          }
      }

    return;
  }

  int
  tgdh_compare_key(TGDH_CONTEXT *ctx[], int num) {
    int i=0;
    BIGNUM *tmp_key=NULL;

    for(i=0; i<num; i++)
      if(ctx[i])
        if(ctx[i]->root->tgdh_nv->key)
          tmp_key=ctx[i]->root->tgdh_nv->key;

    for(i=0; i<num; i++){
        if(ctx[i] != NULL){
            if(BN_cmp(tmp_key, ctx[i]->root->tgdh_nv->key) != 0){
                fprintf(stderr, "()()()()()()()()()()()()()\n");
                return -1;
              }
          }
        else
          {
            printf("***************Some context is empty\n");
          }
      }
    return OK;
  }

  int
  tgdh_check_group_secret (TGDH_CONTEXT *ctx[], int num_users) {
    int i;
    int j;
    int same=TRUE;

    for (i=0; ctx[i]==NULL; i++);
    j=i;
    for (; i < num_users; i++)
      if (ctx[i]!=NULL)
        if (BN_cmp(ctx[j]->group_secret,ctx[i]->group_secret)) {
            same=FALSE;
            DEBUG_PRINT("%d\n",(int)ctx[j]->group_secret);
            DEBUG_PRINT("%d\n",(int)ctx[i]->group_secret);
            break;
          }

    if (same){
        DEBUG_PRINT("Group secret is the same in the entire group :)\n");
        return OK;
      }
    else
      {
        DEBUG_PRINT("Group secret is NOT the same :(\n");
        return GENERIC_THROW;
      }
  }

  /* int_encode: It puts an integer number in stream. Note that the size
   * of the integer number is added to the stream as well.
   */
  void
  int_encode(gka_uchar *stream, gka_uint *pos, gka_uint data) {
    int int_size=(int)htonl(INT_SIZE);
    //printf("encoding %u in %u\n",data,*stream);

    data=htonl(data);
    bcopy (&int_size,stream+*pos,LENGTH_SIZE);
    *pos+=LENGTH_SIZE;
    bcopy (&data    ,stream+*pos,INT_SIZE);
    *pos+=INT_SIZE;
  }

  /* int_decode: It gets an integer number from input->t_data. Note that
   * the size of the integer number is decoded first, and then the
   * actual number is decoded.
   * Returns: 1 succeed.
   *          0 Fails.
   */
  int
  int_decode(const GKA_TOKEN *input, gka_uint *pos, gka_uint *data) {
    int int_size;

    if (input->length  < LENGTH_SIZE+*pos) return 0;
    bcopy (input->t_data+*pos,&int_size,LENGTH_SIZE);
    int_size=(int)ntohl((uint)int_size);
    *pos+=LENGTH_SIZE;

    if (input->length  < int_size+*pos) return 0;
    bcopy (input->t_data+*pos,data,int_size);
    *pos+=int_size;
    *data=ntohl(*data);

    return 1;
  }

  /* string_encode: It puts the valid 'c' string into stream. It first
   * stores the message length (including \0) and the the actual
   * message.
   */
  void
  string_encode (gka_uchar *stream, gka_uint *pos, char *data) {
    int str_len=1;

    /* Note: we are copying the '/0' also */
    str_len+=strlen(data);
    int_encode(stream,pos,str_len);
    bcopy (data,stream+*pos,str_len);
    *pos+=str_len;
  }

  /* string_decode: It restores a valid 'c' string from
   * input->t_data. First the string length is decode (this one should
   * have \0 already), and the actual string.
   * Returns: 1 succeed.
   *          0 Fails.
   */
  int
  string_decode (const GKA_TOKEN *input, gka_uint *pos, char *data) {
    gka_uint str_len;

    if (!int_decode(input,pos,&str_len)) return 0;
    if (input->length  < str_len+*pos) return 0;
    bcopy(input->t_data+*pos,data,str_len);
    *pos+=str_len;

    return 1;
  }

  /**
   * @brief BIGNUM encoding
   * @param [in,out] stream
   * @param [in,out] pos
   * @param [in,out] num
   */
  void
  bn_encode (gka_uchar *stream, gka_uint *pos, BIGNUM *num) {
    gka_uint size;

    size=BN_num_bytes(num);
    assert (size > 0);
    int_encode(stream,pos,size);
    BN_bn2bin(num,stream+*pos);
    *pos+=size;
  }


  /**
   * @brief BIGNUM decoding
   * @pre num != NULL
   * @param [in,out] input
   * @param [in,out] pos
   * @param [in,out] num
   * @return 1 succeed, 0 fail
   */
  int
  bn_decode (const GKA_TOKEN *input, gka_uint *pos, BIGNUM *num) {
    gka_uint size=0;

    if (num == (BIGNUM *) NULL) return 0;
    if (!int_decode(input,pos,&size)) return 0;
    if (size <= 0) return 0;
    if (input->length < size+*pos) return 0;
    BN_bin2bn(input->t_data+*pos,size,num);
    *pos+=size;

    return 1;
  }

  /* tgdh_rand: Generates a new random number of "params->q" bits, using
   *   the default parameters.
   * Returns: A pointer to a dsa structure where the random value
   *          resides.
   *          NULL if an error occurs.
   */
  BIGNUM *tgdh_rand (GKA_KEY *params)
  {
    /* DSA *Random=NULL; */
    int ret=OK;
    BIGNUM *random=NULL;
    int i=0;

    TRY
    {
      random=BN_new();
      if (random == NULL) { ret=MALLOC_ERROR; THROW_;}

      /* The following idea was obtained from dsa_key.c (openssl) */
      i=BN_num_bits(params->params.nid_params.dsa.param_dsa_q);
      for (;;) {
        ret = BN_rand(random, i, 1, 0);
        if (BN_cmp(random, params->params.nid_params.dsa.param_dsa_q) >= 0)
          BN_sub(random, random, params->params.nid_params.dsa.param_dsa_q);
        if (!BN_is_zero(random)) break;
      }
    }
    FINALLY
    {
      if (ret!=OK)
        if (random != NULL) {
          BN_clear_free(random);
          random=NULL;
        }
    }
    ETRY;

    return random;
  }

  /* tgdh_compute_bkey: Computes and returns bkey */
  BIGNUM *tgdh_compute_bkey (BIGNUM *key, GKA_KEY *params)
  {
    int ret=OK;
    BIGNUM *new_bkey = BN_new();
    BN_CTX *bn_ctx = BN_CTX_new();

    TRY
    {
      if (bn_ctx == (BN_CTX *) NULL) {ret=MALLOC_ERROR; THROW_;}
      if (new_bkey == NULL) {ret=MALLOC_ERROR; THROW_;}
      if (key == NULL) {ret=STRUCTURE_ERROR; THROW_;}

      ret = BN_mod(key, key, params->params.nid_params.dsa.param_dsa_q, bn_ctx);

      if (ret != OK) THROW_;

      ret = BN_mod_exp(new_bkey,
                       params->params.nid_params.dsa.param_dsa_g, key,
                       params->params.nid_params.dsa.param_dsa_p, bn_ctx);
    }
    FINALLY
    {
    if (bn_ctx != NULL) BN_CTX_free (bn_ctx);
    if (ret!=OK)
      if (new_bkey != NULL) {
        BN_clear_free(new_bkey);
        new_bkey=NULL;
      }
    }
    ETRY;

    return new_bkey;
  }

  /* tgdh_compute_secret_hash: It computes the hash of the group_secret.
   * Preconditions: ctx->group_secret has to be valid.
   */
  int tgdh_compute_secret_hash (TGDH_CONTEXT *ctx)
  {
    char *tmp_str=NULL;

    tmp_str=BN_bn2hex(ctx->group_secret);
    if (tmp_str==NULL) return CTX_ERROR;

    SHA256((gka_uchar *)tmp_str, (unsigned long)strlen(tmp_str),
        ctx->group_secret_hash);

    free(tmp_str);

    if (ctx->group_secret_hash == (gka_uchar *) NULL) return CTX_ERROR;

    return OK;
  }

  /* tgdh_sign_message: It signs the token using the current user public
   * key scheme. The signature will be appended to the begining of the
   * input token
   */
  int tgdh_sign_message(TGDH_CONTEXT *ctx, GKA_TOKEN *input) {
    int ret=OK;
    EVP_MD_CTX *md_ctx=NULL;
    uint sig_len=0;
    int pkey_len=0;
    gka_uchar *data=NULL;
    uint pos=0;

    TRY
    {
      if (ctx == (TGDH_CONTEXT *) NULL) { ret = CTX_ERROR; THROW_; }
      if (input == (GKA_TOKEN *) NULL)
        {
          fprintf(stderr, "TOKEN NULL=sign\n");
          ret=INVALID_INPUT_TOKEN;
          THROW_;
        }

      md_ctx = EVP_MD_CTX_new();
      if (md_ctx == NULL) { ret = MALLOC_ERROR; THROW_; }

      pkey_len = EVP_PKEY_size(ctx->pkey->private_key);
      data = (gka_uchar *) malloc ((uint)pkey_len+(input->length)+TOTAL_INT);

    #if OPENSSL_VERSION_NUMBER < 0x10100000L
      if (EVP_PKEY_id(ctx->pkey->private_key) == EVP_PKEY_RSA)
        EVP_SignInit (md_ctx, EVP_sha256());
      else if (EVP_PKEY_id(ctx->pkey->private_key) == EVP_PKEY_DSA)
        EVP_SignInit (md_ctx, EVP_sha256());
    #else
      if (EVP_PKEY_id(ctx->pkey->private_key) == EVP_PKEY_RSA)
        EVP_SignInit (md_ctx, EVP_MD_meth_new(NID_sha256, NID_sha256WithRSAEncryption));
      else if (EVP_PKEY_id(ctx->pkey->private_key) == EVP_PKEY_DSA)
        EVP_SignInit (md_ctx, EVP_MD_meth_new(NID_sha256, NID_dsa_with_SHA256));
    #endif
      else {
        ret=INVALID_SIGNATURE_SCHEME;
        THROW_;
      }

      EVP_SignUpdate (md_ctx, input->t_data, input->length);

      /* Encoding size of the signature (an integer), the signature
         length, and then the data */
      ret = EVP_SignFinal (md_ctx, data+TOTAL_INT, &sig_len, ctx->pkey->private_key);
      if (ret == 0) {
    #ifdef SIG_DEBUG
        ERR_print_errors_fp (stderr);
    #endif
        ret=SIGNATURE_ERROR;
        THROW_;
      }
      ret = OK;

      int_encode (data,&pos,sig_len);
      if (pos != TOTAL_INT) { ret=ERROR_INT_DECODE; THROW_; }

      memcpy (data+sig_len+pos, input->t_data, input->length);

      free(input->t_data);
      input->t_data=data;
      input->length+=sig_len+pos;
    }
    FINALLY
    {
      if (md_ctx != NULL) EVP_MD_CTX_free (md_ctx);
      if (ret != OK) free(data);
    }
    ETRY;

    return ret;
  }

  int tgdh_remove_sign(GKA_TOKEN *input, TGDH_SIGN **sign) {
    uint pos=0;
    int ret=OK;
    TGDH_SIGN *signature=*sign;

    TRY
    {
      if (input == (GKA_TOKEN*) NULL){
        fprintf(stderr, "TOKEN NULL:remove\n");
        return INVALID_INPUT_TOKEN;
      }

      if (signature == (TGDH_SIGN*) NULL) {
        signature=(TGDH_SIGN*) malloc (sizeof(TGDH_SIGN));
        if (signature == (TGDH_SIGN*) NULL) return MALLOC_ERROR;
      }

      int_decode (input,&pos,&(signature->length));
      /* Need when restoring the signature in token tgdh_restore_sign */
      if (pos != TOTAL_INT) {ret=ERROR_INT_DECODE; THROW_; }
      if (signature->length+pos > input->length) {
        fprintf(stderr, "length+pos\n");
        ret=INVALID_INPUT_TOKEN;
        THROW_;
      }
      /* No new memory is mallocated just pointers moved around !! */
      signature->signature=input->t_data+pos;
      input->t_data+=signature->length+pos;
      input->length-=signature->length+pos;

      *sign=signature;
      signature=NULL;
    }
    FINALLY
    {
      if (ret!=OK)
        /* If we mallocate the memory, then let's free it */
        if ((*sign==(TGDH_SIGN*)NULL) && (signature != NULL))
          free (signature);
    }
    ETRY;

    return ret;
  }

  int tgdh_restore_sign(GKA_TOKEN *input, TGDH_SIGN **signature) {
    int ret=OK;
    TGDH_SIGN *sign=*signature;

    if (input == (GKA_TOKEN*) NULL){
      fprintf(stderr, "NULL TOKEN: restore\n");
      return INVALID_INPUT_TOKEN;
    }

    if (*signature == (TGDH_SIGN*) NULL) return ret;
    if (input->length+sign->length+TOTAL_INT > MSG_SIZE){
      fprintf(stderr, "size\n");
      return INVALID_INPUT_TOKEN;
    }

    /* No memory needs to be freed ; see tgdh_remove_sign ! */
    input->length+=sign->length+TOTAL_INT;
    input->t_data-=sign->length+TOTAL_INT;

    sign->length=0;
    sign->signature=NULL;
    free (*signature);
    *signature=NULL;

    return ret;
  }

  int tgdh_verify_sign(TGDH_CONTEXT *ctx, TGDH_CONTEXT *new_ctx,
                     GKA_TOKEN *input, GKA_NAME *member_name,
                     TGDH_SIGN *sign)
  {
    int ret=OK;
    EVP_MD_CTX *md_ctx=NULL;
    EVP_PKEY *pubkey=NULL; /* will be set to the public key of member_name */
    KEY_TREE *tmp_tree=NULL;

    TRY
    {
      if (ctx==(TGDH_CONTEXT *)NULL) {ret=CTX_ERROR; THROW_;}
      if (new_ctx==(TGDH_CONTEXT *)NULL) {ret=CTX_ERROR; THROW_;}
      if (input==(GKA_TOKEN*) NULL){
        fprintf(stderr, "TOKEN NULL=vrfy\n");
        ret=INVALID_INPUT_TOKEN;
        THROW_;
      }
      if (sign==(TGDH_SIGN*) NULL) {ret=INVALID_SIGNATURE; THROW_;}
      md_ctx = EVP_MD_CTX_new();
      if (md_ctx == NULL) {ret=MALLOC_ERROR; THROW_;}

      /* Searching for the member and obtainig the public key if needed */
      tmp_tree=tgdh_search_member(new_ctx->root, 4, member_name);
      if (tmp_tree==NULL) {
        ret=MEMBER_NOT_IN_GROUP; THROW_;
      }

      if (tmp_tree->tgdh_nv->member->cert==(X509*)NULL) {
        DEBUG_PRINT("cert is null, looking for certificate of member name %s\n", strcat(member_name,""));
        tmp_tree->tgdh_nv->member->cert = r_gka_get_cert(tmp_tree->tgdh_nv->member->cert, member_name);
        if (tmp_tree->tgdh_nv->member->cert==(X509*)NULL) {ret=INVALID_CERT_FILE; THROW_; }
      }

      if (X509_get_pubkey(tmp_tree->tgdh_nv->member->cert)==NULL) {
        if (X509_get_pubkey(tmp_tree->tgdh_nv->member->cert) ==
            (EVP_PKEY *) NULL) {ret=INVALID_PUBKEY; THROW_; }
      }

      pubkey=X509_get_pubkey(tmp_tree->tgdh_nv->member->cert);
    #if OPENSSL_VERSION_NUMBER < 0x10100000L
      if (EVP_PKEY_id(pubkey) == EVP_PKEY_RSA)
        EVP_VerifyInit (md_ctx, EVP_sha256());
      else if (EVP_PKEY_id(pubkey) == EVP_PKEY_DSA)
        EVP_VerifyInit (md_ctx, EVP_sha256());
    #else
      if (EVP_PKEY_id(pubkey) == EVP_PKEY_RSA)
        EVP_VerifyInit (md_ctx, EVP_MD_meth_new(NID_sha256, NID_sha256WithRSAEncryption));
      else if (EVP_PKEY_id(pubkey) == EVP_PKEY_DSA)
        EVP_VerifyInit (md_ctx, EVP_MD_meth_new(NID_sha256, NID_dsa_with_SHA256));
    #endif
      else {
        ret=INVALID_SIGNATURE_SCHEME;
        THROW_;
      }

      EVP_VerifyUpdate (md_ctx, input->t_data, input->length); // hashes 'length' bytes of data at 't_data' into the verification context md_ctx
      EVP_VerifyFinal (md_ctx, sign->signature, sign->length, pubkey); // verifies the data in md_ctx using the public key pubkey and against 'length' bytes at 'signature'.
      // FIXME: be sure about the VerifyFinal and check it
      if (ret == 0) { // basically that means the signature failed
    #ifdef SIG_DEBUG
        ERR_print_errors_fp (stderr);
    #endif
        DEBUG_PRINT("signature is wrong somehow");
        ret=SIGNATURE_DIFER;
        THROW_;
      }
      ret = OK;
    }
    FINALLY
    {
      if (ret != OK) DEBUG_PRINT("problem in verify_sign");
      if (md_ctx != NULL) EVP_MD_CTX_free (md_ctx);
    }
    ETRY;

    return ret;
  }
