#ifndef libparc_parc_tgdh_tree_h
#define libparc_parc_tgdh_tree_h

/** @file icnet_utils_tgdh_tree.h
 * @brief Function prototypes for the tree structure used by TGDH.
 *
 * This contains the functions related to tree and nodes.
 *
 * @author Pierre-Antoine Rault (rigelk)
 */

#include "parc_tgdh.h"

#include <parc/security/parc_X509Certificate.h>

  /**
   * @struct tgdh_gm_st
   * @brief Group member
   */
  typedef struct tgdh_gm_st {
    GKA_NAME *member_name;     /**< name of the member */
    X509 *cert; /**< X.509 certificate is not null, only if this is a leaf node */
  } TGDH_GM; /**< Type of a group member. */

  /**
   * @struct tgdh_nv
   * @brief Node Values for a node in the tree.
   * @details A node can either be virtual or a real machine.
   *
   * \var int tgdh_nv::index
   * index of this node for encoding and decoding. left child has 2 * index as the index, and right child has 2 * index + 1 has the index
   */
  typedef struct tgdh_nv {
    TGDH_GM   *member;    /**< Member information if this is a leaf node, NULL otherwise */
    uint      index;
    BIGNUM    *key;       /// key if it is on the key-path null otherwise
    BIGNUM    *bkey;      /// blinded key if it is on the co-path or key-path null otherwise
    uint      joinQ;      /// True(1) if this node is joinable, False(0) otherwise
    int       potential;  /// Maximum height of a tree that can be joined to a node in this tree which has this node as a root
    int       height;     /// Height of this tree is not -1, only if this node is root
    int       num_node;   /// For easy encoding and decoding
  } TGDH_NV; /**< Type of a node in the tree. */

  /* Key tree data structures */
  typedef struct key_tree {
    struct key_tree *parent; /* Pointer to the parent */
    struct key_tree *left;   /* Pointer to the left child */
    struct key_tree *right;  /* Pointer to the right child */
    struct key_tree *prev;   /* Pointer to the previous member, if any
                            * This is null, if this node is not
                            * a leaf node
                            */
    struct key_tree *next;   /* Pointer to the next member, if any
                            * This is null, if this node is not
                            * a leaf node
                            */
    struct key_tree *bfs;    /* Special pointer to be used in BFS */
    TGDH_NV *tgdh_nv;        /* Node values if this node is intermediate */
  } KEY_TREE; /**< Type of a tree. */

  /*** METHODS ***/

  /* Frees a TGDH_TREE structure */
  void tgdh_free_tree(KEY_TREE **tree);
  /* Frees a TREE structure */
  void tgdh_free_node(KEY_TREE **tree);
  /* Frees a TGDH_NV structure */
  void tgdh_free_nv(TGDH_NV **nv);
  /* Frees a TGDH_GM structure */
  void tgdh_free_gm(TGDH_GM **gm);

  /* tgdh_search_member: returns the pointer of the previous or the next
 *   member or the first or the last member
 *   if option is 0, this will return the pointer to the previous member
 *   if option is 1, this will return the pointer to the next member
 *     in the above two cases, tree is the starting leaf node in this
 *     searching
 *   if option is 2, this will return the pointer to the first member
 *   if option is 3, this will return the pointer to the last member
 *   if option is 4 and member_name is not null, this will return the
 *     pointer to the node with that name
 */
  KEY_TREE *tgdh_search_member(KEY_TREE *tree, int option,
                               GKA_NAME *member_name );
  /**
   * @brief returns the first fit or worst fit node
   *
   * @param [in] *joiner
   * @param [in] *joinee
   * @param [in] option if is 1, search policy is the best fit
   *                    if is 0, search policy is the first fit
   * @return NULL, if the joiner cannot join to a subtree of joinee
   * Otherwise, it returns the node (first fit or best fit)
   */
  KEY_TREE *tgdh_search_node(KEY_TREE *joiner, KEY_TREE *joinee,
                             int option);
  /* tgdh_search_index: Returns the node having the index as a child */
  KEY_TREE *tgdh_search_index(KEY_TREE *tree, int index);

  /* tgdh_update_index: update index of the input tree by 1
 * index 0 is for the left node
 * index > 0 is for the right node
 */
  void tgdh_update_index(KEY_TREE *tree, int index, int root_index);
  /* Updates potential and joinQ except the leaf node
 * Leaf node should be precomputed before
 */
  void tgdh_update_potential(KEY_TREE *tree);
  /* tgdh_update_index: update joinQ, potential of key_path
 */
  void tgdh_update_key_path(KEY_TREE **tree);

  /* tgdh_copy tree structure, but to finish the real copy, we need to
   call tgdh_dup_tree, which finishes prev and next pointer */
  KEY_TREE *tgdh_copy_tree(KEY_TREE *src);
  /* tgdh_dup_tree finishes the copy process of one tree to
   another... Mainly, it just handles prev and next pointer */
  KEY_TREE *tgdh_dup_tree(KEY_TREE *src);
  /* tgdh_merge merges two trees using tgdh_merge_tree */
  KEY_TREE *tgdh_merge(KEY_TREE *big_tree, KEY_TREE *small_tree);
  /* tgdh_copy_node copies tgdh_nv values of src node to dst node */
  void tgdh_copy_node(KEY_TREE *src, KEY_TREE *dst);
  /* tgdh_swap_bkey swap my null bkey with meaningful bkey from new token */
  void tgdh_swap_bkey(KEY_TREE *src, KEY_TREE *dst);
  /* tgdh_copy_bkey copy meaningful bkey from new token to my null
   token, used for cache update */
  void tgdh_copy_bkey(KEY_TREE *src, KEY_TREE *dst);

  /* tgdh_check_useful checks whether new_ctx has useful information
 * If it has, return 1,
 * else, return 0
 */
  int tgdh_check_useful(KEY_TREE *newtree, KEY_TREE *mytree);
  /* tgdh_init_bfs initializes(nullfies) bfs pointers for each node */
  void tgdh_init_bfs(KEY_TREE *tree);
  /* leaderQ: true if I am the right-most bottom-most subnode of the
     tree, false otherwise
  */
  int leaderQ(KEY_TREE *tree, GKA_NAME *my_name);

#endif // libparc_parc_tgdh_tree_h
