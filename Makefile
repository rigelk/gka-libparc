.PHONY: build test openssl

build: longbow/build
	@make libparc/build

test: build
	@echo "Testing"

longbow/build:
	cd longbow ; mkdir build ; cd build ; cmake .. ; make ; sudo make install

libparc/build:
	cd libparc/parc/security/tgdh/test ; openssl dsaparam -out dsaparam.pem 2048 ; ./gen_dsa.sh 50
	cd libparc ; mkdir build ; cd build ; cmake -DOPENSSL_ROOT_DIR=/usr/local/gka-ssl .. ; make

openssl:
	cd openssl ; \
		./config --prefix=/usr/local/gka-ssl shared no-asm no-ssl2 no-ssl3 -g3 -ggdb -gdwarf-4 -fno-inline -O0 -fno-omit-frame-pointer --debug ; \
		make depend ; \
		make ; \
		sudo make install

