# TGDH

Reimplementation of the Tree Group-Diffie Hellman algorithm, using LongBow and
LibParc structures.

## Usage

You can use the code as shown in the tests at `libparc/parc/security/tgdh/test/test_parc_tgdh.c`.

## Dependencies

TGDH-libparc has been tested in:

- Ubuntu 16.04 (x86_64)
- CentOS 7
- Debian testing
- MacOSX 10.13
- Android >= 7
- iOS 10, 11.

Other platforms and architectures may work.

Build dependencies:

- c99 ( clang / gcc )
- CMake 3.4

Basic dependencies:

- OpenSSL
- pthreads
- Libevent
- LongBow

Documentation dependencies:

- Doxygen

## License

Changes made to the original LibParc code are Copyright © 2018 Pierre-Antoine
Rault for Télécom ParisTech and licensed under either the Apache License
Version 2.0 (APLv2) or the GNU General Public License (GPLv3.0 or later) at
your discretion. See their respective license files at the root of the repository.
